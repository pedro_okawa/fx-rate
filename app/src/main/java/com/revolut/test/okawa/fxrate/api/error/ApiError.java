package com.revolut.test.okawa.fxrate.api.error;

public class ApiError {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
