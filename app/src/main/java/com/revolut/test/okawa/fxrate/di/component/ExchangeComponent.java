package com.revolut.test.okawa.fxrate.di.component;

import com.revolut.test.okawa.fxrate.di.module.ExchangeModule;
import com.revolut.test.okawa.fxrate.di.scope.Fragment;
import com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenter;
import com.revolut.test.okawa.fxrate.ui.exchange.ExchangeFragment;
import com.revolut.test.okawa.fxrate.ui.exchange.ExchangeView;

import dagger.Component;

@Fragment
@Component(dependencies = AppComponent.class, modules = ExchangeModule.class)
public interface ExchangeComponent {

    void inject(ExchangeFragment exchangeFragment);

    ExchangeView providesExchangeView();
    ExchangePresenter providesExchangePresenter();

}
