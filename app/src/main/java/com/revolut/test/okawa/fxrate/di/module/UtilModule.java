package com.revolut.test.okawa.fxrate.di.module;

import com.revolut.test.okawa.fxrate.util.builder.QueryBuilder;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeFactory;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeReferenceFactory;
import com.revolut.test.okawa.fxrate.util.manager.CallManager;
import com.revolut.test.okawa.fxrate.util.manager.NumericConversionManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module that provides all util singletons
 */
@Module
public class UtilModule {

    @Singleton
    @Provides
    public CallManager providesCallManager() {
        return new CallManager();
    }

    @Singleton
    @Provides
    public ExchangeFactory providesExchangeFactory() {
        return new ExchangeFactory();
    }

    @Singleton
    @Provides
    public ExchangeReferenceFactory providesExchangeReferenceFactory(ExchangeFactory exchangeFactory) {
        return new ExchangeReferenceFactory(exchangeFactory);
    }

    @Singleton
    @Provides
    public NumericConversionManager providesNumericConversionManager() {
        return new NumericConversionManager();
    }

    @Singleton
    @Provides
    public QueryBuilder providesQueryBuilder() {
        return new QueryBuilder();
    }

}
