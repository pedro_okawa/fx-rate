package com.revolut.test.okawa.fxrate.util.adapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.TextView;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.model.Exchange;
import com.revolut.test.okawa.fxrate.model.ExchangeReference;

import java.util.Currency;

public class DataBindingAdapter {

    /**
     * Method used to define conversion text on adapter using current item (Exchange reference)
     *
     * @param textView that will hold the value
     * @param exchangeReference item reference that holds currencies of the conversion
     */
    @BindingAdapter("conversion")
    public static void setConversion(TextView textView, ExchangeReference exchangeReference) {
        if(exchangeReference == null || exchangeReference.getBase() == null || exchangeReference.getReference() == null) {
            textView.setText(R.string.not_available);
            return;
        }

        if(exchangeReference.getBase().equals(exchangeReference.getReference())) {
            textView.setVisibility(View.GONE);
            return;
        }

        try {
            String baseSymbol = Currency.getInstance(exchangeReference.getBase().getCurrency()).getSymbol();
            String referenceSymbol = Currency.getInstance(exchangeReference.getReference().getCurrency()).getSymbol();

            String conversion = textView.getContext().getString(R.string.exchange_conversion, baseSymbol, referenceSymbol, exchangeReference.getReference().getConvertedRate());
            textView.setText(conversion);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            textView.setText(R.string.not_available);
        }
    }

    /**
     * Defines preview on top of the screen receiving both objects (Exchange base and reference)
     *
     * @param textView that will hold the value
     * @param base exchange
     * @param reference exchange
     */
    @BindingAdapter({"base", "reference"})
    public static void setPreview(TextView textView, Exchange base, Exchange reference) {
        if(base == null || reference == null) {
            textView.setText(R.string.preview_rate);
            return;
        }

        if(base.equals(reference)) {
            textView.setVisibility(View.GONE);
            return;
        } else {
            textView.setVisibility(View.VISIBLE);
        }

        try {
            String baseSymbol = java.util.Currency.getInstance(reference.getCurrency()).getSymbol();
            String referenceSymbol = java.util.Currency.getInstance(base.getCurrency()).getSymbol();

            String preview = textView.getContext().getString(R.string.exchange_conversion, baseSymbol, referenceSymbol, base.getConvertedRate());
            textView.setText(preview);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            textView.setText(R.string.preview_rate);
        }
    }
}
