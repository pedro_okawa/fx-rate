package com.revolut.test.okawa.fxrate.util.manager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

/**
 * Manager used to convert string to numeric values according to my needs
 */
public class NumericConversionManager {

    private static final String PLACE_ILLEGAL_ARGUMENT = "You can not define a double with negative decimal places";

    private static final double DEFAULT_DOUBLE = 0.00;
    private static final float DEFAULT_FLOAT = 0;
    private static final int DEFAULT_INTEGER = 0;
    private static final long DEFAULT_LONG = 0;

    public double toDouble(String value) {
        try {
            return Double.valueOf(value);
        } catch (NumberFormatException e) {
            return DEFAULT_DOUBLE;
        }
    }

    public float toFloat(String value) {
        try {
            return Float.valueOf(value);
        } catch (NumberFormatException e) {
            return DEFAULT_FLOAT;
        }
    }

    public int toInteger(String value) {
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            return DEFAULT_INTEGER;
        }
    }

    public long toLong(String value) {
        try {
            return Long.valueOf(value);
        } catch (NumberFormatException e) {
            return DEFAULT_LONG;
        }
    }

    /**
     * Method used to round double to the given decimal places
     *
     * @param value
     * @param places
     * @return rounded value
     */
    public double round(double value, int places) {
        if(places < 0) {
            throw new IllegalArgumentException(PLACE_ILLEGAL_ARGUMENT);
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public String fromDouble(double value) {
        return String.format(Locale.getDefault(), value == 0 ? "%.0f" : "%.2f", value);
    }

}
