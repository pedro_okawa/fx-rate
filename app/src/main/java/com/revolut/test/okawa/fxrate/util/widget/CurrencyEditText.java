package com.revolut.test.okawa.fxrate.util.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.revolut.test.okawa.fxrate.util.manager.NumericConversionManager;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Initially this Edit text would be on charge of observe its content, but I changed it to handle
 * currency format as well, because I don't have time enough to create a more elegant View and avoid
 * two TextWatchers
 */
public class CurrencyEditText extends AppCompatEditText {

    private PublishSubject<Double> contentSubject = PublishSubject.create();

    private ContentListener contentListener;
    private NumericConversionManager numericConversionManager;
    private OnRegisterFocusListener onRegisterFocusListener;

    public CurrencyEditText(Context context) {
        super(context);
        initialize();
    }

    public CurrencyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public CurrencyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    /**
     * Initializes content listener and set focus change listener to be used by content listener
     */
    private void initialize() {
        contentListener = new ContentListener();
        numericConversionManager = new NumericConversionManager();
        setOnFocusChangeListener(new OnObservableFocusChangeListener());
        setCustomSelectionActionModeCallback(new ActionModeListener());
    }

    /**
     * Define register focus listener to handle when it starts edit mode
     *
     * @param onRegisterFocusListener
     */
    public void setOnRegisterFocusListener(OnRegisterFocusListener onRegisterFocusListener) {
        this.onRegisterFocusListener = onRegisterFocusListener;
    }

    /**
     * Observable that handles valid edit text content
     *
     * @return Observable to manage content
     */
    public Observable<Double> getContentObservable() {
        return contentSubject;
    }

    /**
     * Focus change listener used to manage when it's on edit mode or not
     */
    private class OnObservableFocusChangeListener implements OnFocusChangeListener {

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if(hasFocus) {
                addTextChangedListener(contentListener);
                if(onRegisterFocusListener != null) {
                    onRegisterFocusListener.onRegister(CurrencyEditText.this);
                }
            } else {
                removeTextChangedListener(contentListener);
                if(onRegisterFocusListener != null) {
                    onRegisterFocusListener.onUnregister();
                }
            }
        }
    }

    /**
     * Listener to handle edit text input and format content
     */
    private class ContentListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            double value = numericConversionManager.toDouble(editable.toString());
            double roundedValue = numericConversionManager.round(value, 2);
            setSelection(Math.min(getSelectionStart(), getText().toString().length()));

            contentSubject.onNext(roundedValue);
        }
    }

    /**
     * Interface used to evaluate when this edit text is registered and unregistered
     */
    public interface OnRegisterFocusListener {

        void onRegister(CurrencyEditText currencyEditText);
        void onUnregister();

    }

    /**
     * Created this class to override all action mode methods and avoid user long press on edit text
     */
    private class ActionModeListener implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {

        }
    }

}
