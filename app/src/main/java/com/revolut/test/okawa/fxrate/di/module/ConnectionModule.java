package com.revolut.test.okawa.fxrate.di.module;

import com.revolut.test.okawa.fxrate.App;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;
import com.revolut.test.okawa.fxrate.util.receiver.NetworkReceiver;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ConnectionModule {

    @Singleton
    @Provides
    public NetworkReceiver providesNetworkReceiver() {
        return new NetworkReceiver();
    }

    @Singleton
    @Provides
    public ConnectionManager providesConnectionManager(App app, NetworkReceiver networkReceiver) {
        return new ConnectionManager(app, networkReceiver);
    }

}
