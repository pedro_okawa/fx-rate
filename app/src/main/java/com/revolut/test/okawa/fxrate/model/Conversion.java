package com.revolut.test.okawa.fxrate.model;

import com.revolut.test.okawa.fxrate.util.manager.NumericConversionManager;

/**
 * Class used to hold conversion values
 */
public class Conversion {

    private double total;
    private double reverse;
    private boolean isRevert = false;
    private NumericConversionManager numericConversionManager = new NumericConversionManager();

    /**
     * Retrieves total according to the current state
     *
     * @param rate
     * @return converted total
     */
    public double getTotal(double rate) {
        return numericConversionManager.round((isRevert ? reverse : total) * rate, 2);
    }

    /**
     * Defines total value for the conversion
     *
     * @param total value without any conversion
     */
    public void setTotal(double total) {
        this.total = reverse = total;
    }

    /**
     * Validates if the current total is the same as total and then apply rate and store other wise
     * set same value
     *
     * @param rate to be applied
     */
    public void setReverse(double rate) {
        if(reverse == total) {
            reverse = total * rate;
            isRevert = true;
        } else {
            reverse = total;
            isRevert = false;
        }
    }
}
