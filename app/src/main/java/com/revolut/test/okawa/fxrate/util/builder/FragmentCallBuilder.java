package com.revolut.test.okawa.fxrate.util.builder;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Builder used to generate fragment call transactions
 */
public class FragmentCallBuilder extends Builder {

    @IntDef({ ADD, REPLACE })
    public @interface FragmentTransition {}

    public static final int ADD = 0x0000;
    public static final int REPLACE = 0x0001;

    private static final int INVALID_CONTAINER_ID = -1;

    private static final String ACTIVITY_NULL_EXCEPTION = "You must call init() before to define an activity to open a fragment";
    private static final String CLASS_NULL_EXCEPTION = "You must call setFragment to define a fragment to be called";
    private static final String CONTAINER_NULL_EXCEPTION = "You must define a view id as container to inflate the fragment";

    private AppCompatActivity activity;
    private boolean addToBackStack;
    private Bundle bundle;
    private Class<? extends Fragment> clazz;

    @FragmentTransition
    private int transition = ADD;

    @IdRes
    private int containerId = INVALID_CONTAINER_ID;

    /**
     * Initializes builder setting up the activity that will hold the fragment
     *
     * @param activity
     * @return Builder instance
     */
    public FragmentCallBuilder init(AppCompatActivity activity) {
        this.activity = activity;
        return this;
    }

    /**
     *
     *
     * @param containerId
     * @return Builder instance
     */
    public FragmentCallBuilder setContainerId(@IdRes int containerId) {
        this.containerId = containerId;
        return this;
    }

    /**
     * Defines fragment to be open
     *
     * @param clazz fragment
     * @return Builder instance
     */
    public FragmentCallBuilder setFragment(Class<? extends Fragment> clazz) {
        this.clazz = clazz;
        return this;
    }

    /**
     * Defines bundle to be used by the opened fragment
     *
     * @param bundle arguments
     * @return Builder instance
     */
    public FragmentCallBuilder setBundle(Bundle bundle) {
        this.bundle = bundle;
        return this;
    }

    /**
     * Define a flag to validate if adds the fragment to back stack or not
     *
     * @param addToBackStack
     * @return Builder instance
     */
    public FragmentCallBuilder addToBackStack(boolean addToBackStack) {
        this.addToBackStack = addToBackStack;
        return this;
    }

    /**
     * Transition type to be applied when instantiate the fragment
     * (It could be {@link #ADD} or {@link #REPLACE})
     *
     * @param transition {@link FragmentTransition}
     * @return Builder instance
     */
    public FragmentCallBuilder setTransition(@FragmentTransition int transition) {
        this.transition = transition;
        return this;
    }

    @Override
    public void build() {
        validate();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        try {
            Fragment fragment = clazz.newInstance();
            fragment.setArguments(bundle);

            String fragmentName = fragment.getClass().getName();

            fragmentTransaction.addToBackStack(addToBackStack ? fragmentName : null);

            if(transition == ADD) {
                fragmentTransaction.add(containerId, fragment);
            } else {
                fragmentTransaction.replace(containerId, fragment);
            }

            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        reset();
    }

    @Override
    protected void validate() {
        if(activity == null) {
            throw new NullPointerException(ACTIVITY_NULL_EXCEPTION);
        }
        if(clazz == null) {
            throw new NullPointerException(CLASS_NULL_EXCEPTION);
        }
        if(containerId == INVALID_CONTAINER_ID) {
            throw new NullPointerException(CONTAINER_NULL_EXCEPTION);
        }
    }

    @Override
    protected void reset() {
        activity = null;
        addToBackStack = false;
        bundle = null;
        clazz = null;
    }
}
