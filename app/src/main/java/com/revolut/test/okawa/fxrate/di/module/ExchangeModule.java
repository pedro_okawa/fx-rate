package com.revolut.test.okawa.fxrate.di.module;

import com.revolut.test.okawa.fxrate.di.scope.Fragment;
import com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenter;
import com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenterImpl;
import com.revolut.test.okawa.fxrate.ui.exchange.ExchangeView;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeReferenceFactory;
import com.revolut.test.okawa.fxrate.util.manager.ApiManager;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import dagger.Module;
import dagger.Provides;

/**
 * Module used to inject all necessary dependencies on
 * {@link com.revolut.test.okawa.fxrate.ui.exchange.ExchangeFragment ExchangeFragment}
 */
@Module
public class ExchangeModule {

    private ExchangeView exchangeView;

    public ExchangeModule(ExchangeView exchangeView) {
        this.exchangeView = exchangeView;
    }

    @Fragment
    @Provides
    public ExchangeView providesExchangeView() {
        return exchangeView;
    }

    @Fragment
    @Provides
    public ExchangePresenter providesExchangePresenter(ApiManager apiManager, ConnectionManager connectionManager, ExchangeReferenceFactory exchangeReferenceFactory, ExchangeView exchangeView) {
        return new ExchangePresenterImpl(apiManager, connectionManager, exchangeReferenceFactory, exchangeView);
    }

}
