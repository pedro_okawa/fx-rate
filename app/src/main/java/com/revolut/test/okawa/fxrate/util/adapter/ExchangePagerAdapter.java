package com.revolut.test.okawa.fxrate.util.adapter;

import android.util.Log;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.databinding.AdapterExchangePageBinding;
import com.revolut.test.okawa.fxrate.model.ExchangeReference;
import com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenter;
import com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenterImpl;
import com.revolut.test.okawa.fxrate.util.manager.NumericConversionManager;
import com.revolut.test.okawa.fxrate.util.widget.BaseViewPager;
import com.revolut.test.okawa.fxrate.util.widget.CurrencyEditText;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Pager adapter used to store Exchange items and observe their values to keep view updated
 */
public class ExchangePagerAdapter extends EndlessPagerAdapter<ExchangeReference, AdapterExchangePageBinding> {

    private PublishSubject<Boolean> requestExchangeSubject = PublishSubject.create();

    private boolean onEditMode;
    private boolean updateData;
    private ExchangePresenter exchangePresenter;
    private InputObserver inputObserver;
    private Map<ExchangeReference, DataObserver> dataObservers;
    private NumericConversionManager numericConversionManager;

    public ExchangePagerAdapter(ExchangePresenter exchangePresenter) {
        super();

        this.exchangePresenter = exchangePresenter;
        dataObservers = new HashMap<>();
        numericConversionManager = new NumericConversionManager();
    }

    @Override
    public void onAttach(BaseViewPager baseViewPager) {
        super.onAttach(baseViewPager);
        baseViewPager.setOffscreenPageLimit(1);
    }

    @Override
    protected void doOnInstantiate(ExchangeReference exchangeReference, AdapterExchangePageBinding dataBinding) {
        defineViews(dataBinding, exchangeReference);
        defineDataObserver(dataBinding, exchangeReference);
        defineFocusListener(dataBinding);
    }

    @Override
    protected void doOnResume(ExchangeReference exchangeReference) {
        requestExchangeSubject.onNext(reset());
    }

    @Override
    protected void doOnDestroy(ExchangeReference exchangeReference) {

    }

    @Override
    protected int layoutToInflate() {
        return R.layout.adapter_exchange_page;
    }

    /**
     * Defines all values related to the UI
     *
     * @param dataBinding
     * @param exchangeReference
     */
    private void defineViews(AdapterExchangePageBinding dataBinding, ExchangeReference exchangeReference) {
        dataBinding.setOnEditMode(isOnEditMode());
        dataBinding.setExchangeReference(exchangeReference);
        updateEditText(dataBinding, exchangeReference);
    }

    /**
     * Defines observer to track exchange references data and update UI
     *
     * @param dataBinding
     * @param exchangeReference
     */
    private void defineDataObserver(final AdapterExchangePageBinding dataBinding, ExchangeReference exchangeReference) {
        DataObserver dataObserver = new DataObserver(dataBinding);

        /* If it's already subscribed remove and onStop (On Complete) */
        if(dataObservers.containsKey(exchangeReference)) {
            dataObservers.remove(exchangeReference).onComplete();
        }

        /* Subscribe */
        exchangeReference
                .getExchangeReferenceSubject()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataObserver);

        /* Add to the map using exchange reference as key */
        dataObservers.put(exchangeReference, dataObserver);
    }

    /**
     * Defines request exchange observable / subject
     *
     * @param requestExchangeSubject
     */
    public void setRequestExchangeSubject(PublishSubject<Boolean> requestExchangeSubject) {
        this.requestExchangeSubject = requestExchangeSubject;
    }

    /**
     * Defines observer to keep tracking user input and associate to data
     *
     * @param currencyEditText
     */
    private void registerInputObserver(CurrencyEditText currencyEditText) {
        unregisterInputObserver();

        inputObserver = new InputObserver();

        currencyEditText
                .getContentObservable()
                .filter(new InputPredicate())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(inputObserver);
    }

    /**
     * Unregister input observer
     */
    private void unregisterInputObserver() {
        if(inputObserver != null) {
            inputObserver.onComplete();
            inputObserver = null;
        }
    }

    /**
     * Defines a listener to check focus on edit text to validate if it's on edit mode or not
     *
     * @param dataBinding
     */
    private void defineFocusListener(AdapterExchangePageBinding dataBinding) {
        OnExchangeFocusListener onExchangeFocusListener = new OnExchangeFocusListener();
        dataBinding.crnExchangePageValue.setOnRegisterFocusListener(onExchangeFocusListener);
    }

    /**
     * Reset total value to zero and notify all items
     *
     * @return true if it needs to  reset by calling again the request otherwise false because it
     * already set values to zero (Is On Edit Mode)
     */
    public boolean reset() {
        if(isOnEditMode()) {
            exchangePresenter.updateValues(ExchangePresenterImpl.DEFAULT_VALUE);
            return false;
        }
        return true;
    }

    /**
     * Custom method to update edit text because it sets a flag to filter input observable and
     * avoid call when it's not necessary
     *
     * @param dataBinding
     * @param exchangeReference
     */
    private void updateEditText(AdapterExchangePageBinding dataBinding, ExchangeReference exchangeReference) {
        setUpdateData(true);

        String content = dataBinding.crnExchangePageValue.getText().toString();
        double valueTyped = numericConversionManager.toDouble(content);
        double valueExchange = exchangePresenter.getConvertedTotal(exchangeReference.getBase().getRate());

        /*
            Validates it's not on edit mode and it's the current item or
            typed value is different from stored value and stored value different of zero or
            value typed is empty (In this case add zero)
         */
        if((!(isOnEditMode() && isCurrentItem(exchangeReference)) && (valueTyped != valueExchange))
                || ((valueTyped != valueExchange) && (valueExchange == 0)) || content.isEmpty()) {
            dataBinding.crnExchangePageValue.setText(numericConversionManager.fromDouble(valueExchange));
        }

        setUpdateData(false);
    }

    /**
     * Reverts the exchange value setting the converted value to total
     */
    private void revert() {
        if(!hasChangedScreen()) {
            exchangePresenter.revertValue(getCurrentItem().getBase().getRate());
        }
    }

    public boolean isUpdateData() {
        return updateData;
    }

    private void setUpdateData(boolean updateData) {
        this.updateData = updateData;
    }

    public boolean isOnEditMode() {
        return onEditMode;
    }

    private void setOnEditMode(boolean onEditMode) {
        this.onEditMode = onEditMode;
    }

    /**
     * Class used to observe input on adapter edit text
     */
    private class InputObserver implements Observer<Double> {

        private Disposable disposable;

        @Override
        public void onSubscribe(Disposable disposable) {
            this.disposable = disposable;
        }

        @Override
        public void onNext(Double value) {
            exchangePresenter.updateValues(value);
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onComplete() {
            disposable.dispose();
        }
    }

    /**
     * Class used to handle data content and assign to the UI
     */
    public class DataObserver implements Observer<ExchangeReference> {

        private AdapterExchangePageBinding dataBinding;
        private Disposable disposable;

        public DataObserver(AdapterExchangePageBinding dataBinding) {
            this.dataBinding = dataBinding;
        }

        @Override
        public void onSubscribe(Disposable disposable) {
            this.disposable = disposable;
        }

        @Override
        public void onNext(ExchangeReference exchangeReference) {
            defineViews(dataBinding, exchangeReference);
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onComplete() {
            disposable.dispose();
        }
    }

    /**
     * Class used as condition to filter input observable and avoid call edit text text watcher when
     * it's updating UI without used input and try to make it run better and leaner, but I think
     * it's possible to find more updates to improve it
     */
    private class InputPredicate implements Predicate<Double> {

        @Override
        public boolean test(Double value) throws Exception {
            return !isUpdateData() && isOnEditMode();
        }
    }

    /**
     * Interface used to manages EditText focus
     */
    private class OnExchangeFocusListener implements CurrencyEditText.OnRegisterFocusListener {

        @Override
        public void onRegister(CurrencyEditText currencyEditText) {
            /* Register input observer (Only one per adapter) */
            registerInputObserver(currencyEditText);
            /* Set it to edit mode */
            setOnEditMode(true);
            /* When edit text listener is registered we check if it should be reverted to convert */
            revert();
            /* Notify Exchange Presenter to make a new request */
            requestExchangeSubject.onNext(true);
        }

        @Override
        public void onUnregister() {
            /* Unregister input observer */
            unregisterInputObserver();
            /* Reset edit mode variable */
            setOnEditMode(false);
        }

    }
}