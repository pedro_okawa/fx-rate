package com.revolut.test.okawa.fxrate.di.module;

import com.revolut.test.okawa.fxrate.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module used to define the App as a Singleton
 */
@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Singleton
    @Provides
    public App providesApp() {
        return app;
    }

}
