package com.revolut.test.okawa.fxrate.util.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.revolut.test.okawa.fxrate.util.widget.BaseViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BasePagerAdapter<T, K extends ViewDataBinding> extends PagerAdapter {

    private static final String NULL_BASE_PAGER_EXCEPTION = "You must call setAdapter on your BaseViewPager";
    private static final String ITEM_OUT_OF_BOUNDS_EXCEPTION = "You're trying to retrieve an invalid item position: ";

    private List<T> data;
    private Map<T, K> dataBindings;
    private BaseViewPager baseViewPager;

    private int previousPosition = 0;

    public BasePagerAdapter() {
        super();
        initialize(new ArrayList<T>());
    }

    public BasePagerAdapter(List<T> data) {
        super();
        initialize(data);
    }

    private void initialize(List<T>  data) {
        this.data = data;
        dataBindings = new HashMap<>();
    }

    /**
     * Method called when view is instantiated with the necessary data
     * ({@link T Item} and {@link K DataBinding})
     *
     * @param t Item
     * @param k DataBinding
     */
    protected abstract void doOnInstantiate(T t, K k);

    /**
     * Method called when a page is selected
     *
     * @param t Item
     */
    protected abstract void doOnResume(T t);

    /**
     * Method called after execute unbind on current DataBinding and removed the view
     *
     * @param t Item from the deleted view
     */
    protected abstract void doOnDestroy(T t);

    /**
     * Layout of the adapter to be inflated
     *
     * @return layout id
     */
    @LayoutRes
    protected abstract int layoutToInflate();

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(container.getContext());
        K dataBinding = DataBindingUtil.inflate(layoutInflater, layoutToInflate(), container, false);
        T item = getItem(position);

        container.addView(dataBinding.getRoot());
        dataBindings.put(item, dataBinding);

        doOnInstantiate(item, dataBinding);

        return dataBinding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        K dataBinding = dataBindings.remove(getItem(position));

        if(dataBinding != null) {
            dataBinding.unbind();
        }

        container.removeView((View) object);

        doOnDestroy(getItem(position));
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * Method call when an adapter is defined inside a View Pager
     *
     * @param baseViewPager to receive the adapter
     */
    public void onAttach(BaseViewPager baseViewPager) {
        this.baseViewPager = baseViewPager;
        this.baseViewPager.addOnPageChangeListener(new BasePageListener());
    }

    /**
     * Adds a data set to the adapter
     *
     * @param data collection
     */
    public void setData(List<T> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    /**
     * Allows user to add an item to the adapter
     *
     * @param item to be added
     */
    public void add(T item) {
        data.add(item);
        notifyDataSetChanged();
    }

    /**
     * Removes an item from the adapter data collection
     *
     * @param item
     * @return
     */
    public boolean remove(T item) {
        boolean removed = data.remove(item);

        if(removed) {
            notifyDataSetChanged();
        }

        return removed;
    }

    /**
     * Retrieve the item for the given position
     *
     * @param position
     * @return item selected by the position or throw an index out of bounds exception if it's an invalid position
     */
    public T getItem(int position) {
        if(position < POSITION_UNCHANGED || position >= data.size()) {
            new ArrayIndexOutOfBoundsException(ITEM_OUT_OF_BOUNDS_EXCEPTION.concat(String.valueOf(position))).printStackTrace();
            return null;
        }

        return data.get(position);
    }

    /**
     * Retreive databinding for the given position
     *
     * @param position
     * @return databinding selected by the position or throw an index out of bounds exception if it's an invalid position
     */
    public K getDataBinding(int position) {
        if(position < POSITION_UNCHANGED || position >= dataBindings.size()) {
            new ArrayIndexOutOfBoundsException(ITEM_OUT_OF_BOUNDS_EXCEPTION.concat(String.valueOf(position))).printStackTrace();
            return null;
        }
        return dataBindings.get(position);
    }

    /**
     * Retrieves current item displayed on screen
     *
     * @return current item
     */
    public T getCurrentItem() {
        if(baseViewPager == null) {
            throw new NullPointerException(NULL_BASE_PAGER_EXCEPTION);
        }
        return getItem(baseViewPager.getCurrentItem());
    }

    /**
     * Retrieves current item displayed on screen
     *
     * @return current item
     */
    public T getPreviousItem() {
        return getItem(previousPosition);
    }

    /**
     * Retrieves data collection
     *
     * @return data list
     */
    public List<T> getData() {
        return data;
    }

    /**
     * Retrieves view pager holding the adapter
     *
     * @return base view pager
     */
    public BaseViewPager getBaseViewPager() {
        return baseViewPager;
    }

    public boolean isCurrentItem(T t) {
        if(baseViewPager == null) {
            return false;
        }
        return t == getItem(baseViewPager.getCurrentItem());
    }

    public boolean hasChangedScreen() {
        if(baseViewPager == null) {
            return false;
        }
        return previousPosition != baseViewPager.getCurrentItem();
    }

    private class BasePageListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            previousPosition = position;
        }

        @Override
        public void onPageSelected(int position) {
            T t = getItem(position);

            if(t == null) {
                return;
            }

            doOnResume(t);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
