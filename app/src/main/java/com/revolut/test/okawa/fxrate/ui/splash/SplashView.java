package com.revolut.test.okawa.fxrate.ui.splash;

import com.revolut.test.okawa.fxrate.ui.base.BaseView;

/**
 * View used to establish communication between
 * {@link com.revolut.test.okawa.fxrate.presenter.splash.SplashPresenter} and
 * {@link SplashActivity}
 */
public interface SplashView extends BaseView {

}
