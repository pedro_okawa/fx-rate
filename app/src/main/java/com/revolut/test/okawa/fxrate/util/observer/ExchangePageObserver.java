package com.revolut.test.okawa.fxrate.util.observer;

import io.reactivex.Observer;

public abstract class ExchangePageObserver implements Observer<Boolean> {

    protected abstract void onResetExternal();
    protected abstract void onResetInternal();

    @Override
    public final void onNext(Boolean needToReset) {
        if(needToReset) {
            onResetExternal();
        } else {
            onResetInternal();
        }
    }

    @Override
    public final void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public final void onComplete() {

    }
}
