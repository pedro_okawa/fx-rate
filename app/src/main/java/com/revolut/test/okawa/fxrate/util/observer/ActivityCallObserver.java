package com.revolut.test.okawa.fxrate.util.observer;

import com.revolut.test.okawa.fxrate.util.builder.ActivityCallBuilder;

import io.reactivex.Observer;

public abstract class ActivityCallObserver implements Observer<ActivityCallBuilder> {

    @Override
    public final void onNext(ActivityCallBuilder activityCallBuilder) {
        activityCallBuilder.build();
    }

    @Override
    public final void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public final void onComplete() {

    }
}
