package com.revolut.test.okawa.fxrate.util.observer;

import android.content.Context;

import com.revolut.test.okawa.fxrate.model.ExchangeReference;

import java.util.List;

public abstract class ExchangeReferenceRequestObserver extends RequestObserver<List<ExchangeReference>> {

    public ExchangeReferenceRequestObserver(Context context) {
        super(context);
    }

    protected abstract void onReceiveExchangeReferences(List<ExchangeReference> exchangeReferences);

    @Override
    protected void onReceive(List<ExchangeReference> exchangeReferences) {
        if(exchangeReferences.isEmpty()) {
            return;
        }

        onReceiveExchangeReferences(exchangeReferences);
    }
}
