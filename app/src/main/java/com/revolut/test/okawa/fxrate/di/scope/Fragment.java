package com.revolut.test.okawa.fxrate.di.scope;

import javax.inject.Scope;

/**
 * Scope used to define components on Fragment level
 */
@Scope
public @interface Fragment {
}
