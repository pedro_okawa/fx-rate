package com.revolut.test.okawa.fxrate.util.factory;

import com.revolut.test.okawa.fxrate.util.api.RxCallAdapterWrapper;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class RxErrorCallAdapterFactory extends CallAdapter.Factory {

    private ConnectionManager connectionManager;
    private RxJava2CallAdapterFactory rxJava2CallAdapterFactory;

    public RxErrorCallAdapterFactory(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        this.rxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create();
    }

    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        return new RxCallAdapterWrapper(rxJava2CallAdapterFactory.get(returnType, annotations, retrofit), connectionManager, retrofit);
    }
}