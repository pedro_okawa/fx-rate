package com.revolut.test.okawa.fxrate.util.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder used to generate API queries
 */
public class QueryBuilder {

    private static final String QUERY_NULL_EXCEPTION = "You must call init() first";

    private Map<String, String> query;

    public static <T, K> Map<T, K> empty() {
        return new HashMap<>();
    }

    public QueryBuilder init() {
        query = new HashMap<>();
        return this;
    }

    public QueryBuilder addQuery(String key, String value) {
        validateQuery();
        query.put(key, value);
        return this;
    }

    public QueryBuilder addQuery(String key, int value) {
        return addQuery(key, String.valueOf(value));
    }

    public QueryBuilder addQuery(String key, long value) {
        return addQuery(key, String.valueOf(value));
    }

    public QueryBuilder addQuery(String key, double value) {
        return addQuery(key, String.valueOf(value));
    }

    public QueryBuilder addQuery(String key, float value) {
        return addQuery(key, String.valueOf(value));
    }

    public QueryBuilder addQuery(String key, char[] value) {
        return addQuery(key, String.valueOf(value));
    }

    public QueryBuilder addQuery(String key, boolean value) {
        return addQuery(key, String.valueOf(value));
    }

    public QueryBuilder addQuery(String key, String ... values) {
        return addQuery(key, serializeArray(values));
    }

    public QueryBuilder addQuery(String key, int ... values) {
        return addQuery(key, serializeArray(values));
    }

    public QueryBuilder addQuery(String key, long ... values) {
        return addQuery(key, serializeArray(values));
    }

    public QueryBuilder addQuery(String key, double ... values) {
        return addQuery(key, serializeArray(values));
    }

    public QueryBuilder addQuery(String key, float ... values) {
        return addQuery(key, serializeArray(values));
    }

    public QueryBuilder addQuery(String key, char[] ... values) {
        return addQuery(key, serializeArray(values));
    }

    public Map<String, String> build() {
        validateQuery();
        Map<String, String> result = query;
        query = null;
        return result;
    }

    /**
     * Validates if the Query can be generated
     */
    private void validateQuery() {
        if(query == null) {
            throw new NullPointerException(QUERY_NULL_EXCEPTION);
        }
    }

    /**
     * This method allows convert a group of items into a String comma separated
     *
     * @param objects to be converted
     * @param <T> type of the objects
     * @return string converted
     */
    private <T> String serializeArray(T ... objects) {
        String separator = "";
        StringBuilder stringBuilder = new StringBuilder();
        for(Object object : objects) {
            stringBuilder.append(separator).append(object);
            separator = ",";
        }
        return stringBuilder.toString();
    }

}
