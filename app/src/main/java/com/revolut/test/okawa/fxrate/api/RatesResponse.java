package com.revolut.test.okawa.fxrate.api;

import com.revolut.test.okawa.fxrate.model.Currency;

import java.util.Date;
import java.util.Map;

/**
 * Reference to a rate based on a specific currency
 */
public class RatesResponse {

    @Currency
    private String base;
    private Date date;
    private Map<String, Double> rates;

    public String getBase() {
        return base;
    }

    public Date getDate() {
        return date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }
}
