package com.revolut.test.okawa.fxrate.util.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.revolut.test.okawa.fxrate.App;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import javax.inject.Inject;

public class NetworkReceiver extends BroadcastReceiver {

    @Inject
    ConnectionManager connectionManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        ((App)context.getApplicationContext()).getAppComponent().inject(this);
        connectionManager.defineConnectionState(context);
    }
}
