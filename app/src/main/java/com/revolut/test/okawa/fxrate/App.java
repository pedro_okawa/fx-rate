package com.revolut.test.okawa.fxrate;

import android.app.Application;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.revolut.test.okawa.fxrate.di.component.AppComponent;
import com.revolut.test.okawa.fxrate.di.component.DaggerAppComponent;
import com.revolut.test.okawa.fxrate.di.module.AppModule;
import com.revolut.test.okawa.fxrate.di.module.ConnectionModule;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Application class that initializes Dagger App component and Fabric
 */
public class App extends Application {

    @Inject
    ConnectionManager connectionManager;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        /* Initialize all components */
        appComponent = initializeComponent();

        /* Initialize crashlytics  */
        Fabric.with(this, new Crashlytics());

        /*
         Register broadcast receiver
         Registered programmatically because connectivity_action is deprecated when registered on manifest
         */
        connectionManager.registerConnectivityMonitor();
    }

    /**
     * Initializes all dagger components
     * This method was created only to be override by AppTest and inject test components
     */
    protected AppComponent initializeComponent() {
        /* Define app component */
        AppComponent appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .connectionModule(new ConnectionModule())
                .build();

        /* Injected to receive a connection manager instance */
        appComponent.inject(this);

        return appComponent;
    }

    @NonNull
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
