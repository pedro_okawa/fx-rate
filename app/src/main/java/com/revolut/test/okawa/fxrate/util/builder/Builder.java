package com.revolut.test.okawa.fxrate.util.builder;

/**
 * Base class used to abstract some Builder methods used
 */
public abstract class Builder {

    /**
     * Build, call and finish the request
     */
    public abstract void build();

    /**
     * Validates if the builder is correctly filled
     */
    protected abstract void validate();

    /**
     * Reset all values stored on last call
     */
    protected abstract void reset();

}
