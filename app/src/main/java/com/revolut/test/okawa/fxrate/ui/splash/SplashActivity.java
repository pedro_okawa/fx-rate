package com.revolut.test.okawa.fxrate.ui.splash;

import android.content.Context;
import android.support.annotation.NonNull;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.databinding.ActivitySplashBinding;
import com.revolut.test.okawa.fxrate.di.component.AppComponent;
import com.revolut.test.okawa.fxrate.di.component.DaggerSplashComponent;
import com.revolut.test.okawa.fxrate.di.module.SplashModule;
import com.revolut.test.okawa.fxrate.presenter.splash.SplashPresenter;
import com.revolut.test.okawa.fxrate.ui.base.BaseActivity;

import javax.inject.Inject;

/**
 * Initial activity of the application
 */
public class SplashActivity extends BaseActivity<ActivitySplashBinding> implements SplashView {

    @Inject
    SplashPresenter splashPresenter;

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_splash;
    }

    @Override
    protected void injectDependencies(@NonNull AppComponent appComponent) {
        DaggerSplashComponent
                .builder()
                .appComponent(appComponent)
                .splashModule(new SplashModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void initialize(@NonNull ActivitySplashBinding dataBinding) {
        splashPresenter.initialize(dataBinding);
    }

    @Override
    protected void onStart() {
        super.onStart();
        splashPresenter.onStart();
    }

    @Override
    protected void onStop() {
        splashPresenter.onStop();
        super.onStop();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
