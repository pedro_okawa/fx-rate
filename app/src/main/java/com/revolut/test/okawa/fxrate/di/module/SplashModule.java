package com.revolut.test.okawa.fxrate.di.module;

import com.revolut.test.okawa.fxrate.di.scope.Activity;
import com.revolut.test.okawa.fxrate.presenter.splash.SplashPresenter;
import com.revolut.test.okawa.fxrate.presenter.splash.SplashPresenterImpl;
import com.revolut.test.okawa.fxrate.ui.splash.SplashActivity;
import com.revolut.test.okawa.fxrate.ui.splash.SplashView;
import com.revolut.test.okawa.fxrate.util.manager.CallManager;

import dagger.Module;
import dagger.Provides;

/**
 * Module used to inject all necessary dependencies on
 * {@link SplashActivity SplashActivity}
 */
@Module
public class SplashModule {

    private SplashView splashView;

    public SplashModule(SplashView splashView) {
        this.splashView = splashView;
    }

    @Activity
    @Provides
    public SplashView providesSplashView() {
        return splashView;
    }

    @Activity
    @Provides
    public SplashPresenter providesSplashPresenter(CallManager callManager, SplashView baseView) {
        return new SplashPresenterImpl(callManager, baseView);
    }

}
