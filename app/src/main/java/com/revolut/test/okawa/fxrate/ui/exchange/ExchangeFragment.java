package com.revolut.test.okawa.fxrate.ui.exchange;

import android.support.annotation.NonNull;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.databinding.FragmentExchangeBinding;
import com.revolut.test.okawa.fxrate.di.component.AppComponent;
import com.revolut.test.okawa.fxrate.di.component.DaggerExchangeComponent;
import com.revolut.test.okawa.fxrate.di.module.ExchangeModule;
import com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenter;
import com.revolut.test.okawa.fxrate.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

/**
 * Exchange fragment that injects an instance of {@link ExchangePresenter} to make all the logic
 */
public class ExchangeFragment extends BaseFragment<FragmentExchangeBinding> implements ExchangeView {

    public static final String BUNDLE_CURRENCIES = "BUNDLE_CURRENCIES";

    @Inject
    ExchangePresenter exchangePresenter;

    @Override
    protected int layoutToInflate() {
        return R.layout.fragment_exchange;
    }

    @Override
    protected void injectDependencies(@NonNull AppComponent appComponent) {
        DaggerExchangeComponent
                .builder()
                .appComponent(appComponent)
                .exchangeModule(new ExchangeModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected void initialize(@NonNull FragmentExchangeBinding dataBinding) {
        List<String> currencies = getArguments().getStringArrayList(BUNDLE_CURRENCIES);
        exchangePresenter.defineCurrencies(currencies);
        exchangePresenter.initialize(dataBinding);
    }

    /**
     * Starts api requests every time starts this fragment
     */
    @Override
    public void onStart() {
        super.onStart();
        exchangePresenter.onStart();
    }

    /**
     * Dispose all requests, in this case only the api request (These values are going to be
     * shown on UI and always updated values, so no need to keep it running after this fragment
     * being stopped).
     */
    @Override
    public void onDestroy() {
        exchangePresenter.onStop();
        super.onDestroy();
    }
}
