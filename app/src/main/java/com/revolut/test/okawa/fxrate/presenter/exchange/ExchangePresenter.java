package com.revolut.test.okawa.fxrate.presenter.exchange;

import com.revolut.test.okawa.fxrate.databinding.FragmentExchangeBinding;
import com.revolut.test.okawa.fxrate.model.Exchange;
import com.revolut.test.okawa.fxrate.presenter.base.BasePresenter;

import java.util.List;

/**
 * Exchange presenter interface used to communicate
 * {@link com.revolut.test.okawa.fxrate.ui.exchange.ExchangeFragment } and
 * {@link ExchangePresenterImpl}
 */
public interface ExchangePresenter extends BasePresenter<FragmentExchangeBinding> {

    /**
     * Defines the currencies used to be validated on Exchange Fragment
     * These are the limited list of currencies used to request on API
     *
     * @param currencies to be requested
     */
    void defineCurrencies(List<String> currencies);

    /**
     * Disposes previous request and starts call again
     */
    void resetRequest();

    /**
     * Retrieves input total value converted by the given rate
     *
     * @return value converted
     */
    double getConvertedTotal(double rate);

    /**
     * Update all data values at once
     *
     * @param value to be converted
     */
    void updateValues(double value);

    /**
     *
     * @param rate
     */
    void revertValue(double rate);

}
