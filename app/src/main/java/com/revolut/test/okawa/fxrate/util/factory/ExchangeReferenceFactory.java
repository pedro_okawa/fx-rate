package com.revolut.test.okawa.fxrate.util.factory;

import com.revolut.test.okawa.fxrate.model.Currency;
import com.revolut.test.okawa.fxrate.model.Exchange;
import com.revolut.test.okawa.fxrate.model.ExchangeReference;

import java.util.ArrayList;
import java.util.List;

public class ExchangeReferenceFactory {

    private static final String EXCHANGE_REFERENCE_NULL_EXCEPTION = "You must call init() before you generate an ExchangeReference";

    private ExchangeFactory exchangeFactory;
    private ExchangeReference exchangeReference;

    public ExchangeReferenceFactory(ExchangeFactory exchangeFactory) {
        this.exchangeFactory = exchangeFactory;
    }

    public List<ExchangeReference> standardList(@Currency List<String> currencies) {
        @Currency String [] convertedList = new String[currencies.size()];
        currencies.toArray(convertedList);
        return standardList(convertedList);
    }

    public List<ExchangeReference> standardList(@Currency String ... currencies) {
        List<ExchangeReference> result = new ArrayList<>();

        for(String currency : currencies) {
            result.add(standard(currency));
        }

        return result;
    }

    public ExchangeReference standard(@Currency String currency) {
        return init()
                .setBase(exchangeFactory.standard(currency))
                .setReference(exchangeFactory.standard(currency))
                .generate();
    }

    public ExchangeReferenceFactory init() {
        exchangeReference = new ExchangeReference();
        return this;
    }

    public ExchangeReferenceFactory setBase(Exchange exchange) {
        validate();
        exchangeReference.setBase(exchange);
        return this;
    }

    public ExchangeReferenceFactory setBase(@Currency String currency, double rate) {
        validate();
        exchangeReference.setBase(generateExchange(currency, rate));
        return this;
    }

    public ExchangeReferenceFactory setBase(@Currency String currency) {
        validate();
        exchangeReference.setBase(generateExchange(currency, Exchange.BASE_RATE));
        return this;
    }

    public ExchangeReferenceFactory setReference(Exchange exchange) {
        validate();
        exchangeReference.setReference(exchange);
        return this;
    }

    public ExchangeReferenceFactory setReference(@Currency String currency, double rate) {
        validate();
        exchangeReference.setReference(generateExchange(currency, rate));
        return this;
    }

    public ExchangeReferenceFactory setReference(@Currency String currency) {
        validate();
        exchangeReference.setReference(generateExchange(currency, Exchange.BASE_RATE));
        return this;
    }

    public ExchangeReference generate() {
        validate();
        ExchangeReference generatedExchangeReference = exchangeReference;
        exchangeReference = null;
        return generatedExchangeReference;
    }

    private Exchange generateExchange(@Currency String currency, double rate) {
        exchangeFactory.init();
        exchangeFactory.setCurrency(currency);
        exchangeFactory.setRate(rate);
        return exchangeFactory.generate();
    }

    private void validate() {
        if(exchangeReference == null) {
            throw new NullPointerException(EXCHANGE_REFERENCE_NULL_EXCEPTION);
        }
    }
}
