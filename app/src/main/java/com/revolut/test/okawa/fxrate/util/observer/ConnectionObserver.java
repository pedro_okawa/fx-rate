package com.revolut.test.okawa.fxrate.util.observer;

import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import io.reactivex.Observer;

public abstract class ConnectionObserver implements Observer<Integer> {

    protected abstract void onReconnected();
    protected abstract void onDisconnected();

    @Override
    public final void onNext(Integer value) {
        if(value == ConnectionManager.ONLINE) {
            onReconnected();
        } else if(value == ConnectionManager.OFFLINE) {
            onDisconnected();
        }
    }

    @Override
    public final void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public final void onComplete() {

    }
}
