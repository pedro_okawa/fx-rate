package com.revolut.test.okawa.fxrate.util.adapter;

import android.databinding.ViewDataBinding;
import android.support.v4.view.PagerAdapter;
import android.view.ViewGroup;

import com.revolut.test.okawa.fxrate.util.widget.BaseViewPager;

import java.util.List;

/**
 * Endless pager adapter. It's basically a Regular Pager adapter with a monstrous, enormous
 * and giant count number, in this case {@link Integer#MAX_VALUE max value}.
 * It's not using Fragment Pager Adapter because it was consuming a lot of memory, causing some
 * OutOfMemory crashes on {@link android.support.v4.view.ViewPager#setCurrentItem(int, boolean)}
 *
 * Another responsibility is to manage View lifecycle, when it creates and when it destroys
 *
 * @param <T> data type
 * @param <K> data binding
 */
public abstract class EndlessPagerAdapter<T, K extends ViewDataBinding> extends BasePagerAdapter<T, K> {

    public EndlessPagerAdapter() {
        super();
    }

    public EndlessPagerAdapter(List<T> data) {
        super(data);
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    public int getRealCount() {
        return getData().size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, getRealPosition(position));
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, getRealPosition(position), object);
    }

    @Override
    public T getItem(int position) {
        return super.getItem(getRealPosition(position));
    }

    /**
     * Retrieves the start position for the view pager, otherwise this will not have this awesome
     * endless carousel effect. So do not forget to call setCurrentPosition with this
     * method as parameter on your ViewPager.
     *
     * ### NEVER FORGET ###
     * ###    NEVER     ###
     *
     * viewPager.setCurrentPosition(getStartPosition(), false);
     *
     * @return start position for the view pager
     */
    public int getStartPosition() {
        return getCount() / 2;
    }

    /**
     * Returns real position of the list
     *
     * @return data item position
     */
    public int getRealPosition(int position) {
        if(getData().isEmpty()) {
            return PagerAdapter.POSITION_NONE;
        }

        return position % getData().size();
    }
}
