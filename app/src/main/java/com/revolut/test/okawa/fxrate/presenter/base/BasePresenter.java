package com.revolut.test.okawa.fxrate.presenter.base;

import android.databinding.ViewDataBinding;

/**
 * Base presenter that handles basic lifecycle (when it {@link #initialize(ViewDataBinding)}
 * and when it {@link #onStop()})
 *
 * @param <T> must be an instance of an object related to {@link ViewDataBinding ViewDataBinding}
 */
public interface BasePresenter<T extends ViewDataBinding> {

    /**
     * Initializes the presenter with a given databinding
     *
     * @param t databinding
     */
    void initialize(T t);

    /**
     * It calls all methods necessary to be initialized on start lifecycle
     */
    void onStart();

    /**
     * Must be called when it finishes and call onStop to dispose all references
     */
    void onStop();

}
