package com.revolut.test.okawa.fxrate.model;


import android.support.annotation.StringDef;

/**
 * Group of all available currencies on API
 */
@StringDef({ Currency.AUD, Currency.BGN, Currency.BRL, Currency.CAD, Currency.CHF, Currency.CNY,
        Currency.CZK, Currency.DKK, Currency.EUR, Currency.GBP, Currency.HKD, Currency.HRK,
        Currency.HUF, Currency.IDR, Currency.ILS, Currency.INR, Currency.JPY, Currency.KRW,
        Currency.MXN, Currency.MYR, Currency.NOK, Currency.NZD, Currency.PHP, Currency.PLN,
        Currency.RON, Currency.RUB, Currency.SEK, Currency.SGD, Currency.THB, Currency.TRY,
        Currency.USD, Currency.TRY, Currency.ZAR })
public @interface Currency {

    String AUD = "AUD";
    String BGN = "BGN";
    String BRL = "BRL";
    String CAD = "CAD";
    String CHF = "CHF";
    String CNY = "CNY";
    String CZK = "CZK";
    String DKK = "DKK";
    String EUR = "EUR";
    String GBP = "GBP";
    String HKD = "HKD";
    String HRK = "HRK";
    String HUF = "HUF";
    String IDR = "IDR";
    String ILS = "ILS";
    String INR = "INR";
    String JPY = "JPY";
    String KRW = "KRW";
    String MXN = "MXN";
    String MYR = "MYR";
    String NOK = "NOK";
    String NZD = "NZD";
    String PHP = "PHP";
    String PLN = "PLN";
    String RON = "RON";
    String RUB = "RUB";
    String SEK = "SEK";
    String SGD = "SGD";
    String THB = "THB";
    String TRY = "TRY";
    String USD = "USD";
    String ZAR = "TRY";

}
