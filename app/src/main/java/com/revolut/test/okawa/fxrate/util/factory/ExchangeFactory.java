package com.revolut.test.okawa.fxrate.util.factory;

import com.revolut.test.okawa.fxrate.model.Currency;
import com.revolut.test.okawa.fxrate.model.Exchange;

/**
 * Factory used to generate Exchange objects
 */
public class ExchangeFactory {

    private static final String EXCHANGE_NULL_EXCEPTION = "You must call init() before to define an object";

    private Exchange exchange;

    public Exchange standard(@Currency String currency) {
        return init()
                .setCurrency(currency)
                .setRate(Exchange.BASE_RATE)
                .generate();
    }

    public ExchangeFactory init() {
        exchange = new Exchange();
        return this;
    }

    public ExchangeFactory setCurrency(@Currency String currency) {
        validate();
        exchange.setCurrency(currency);
        return this;
    }

    public ExchangeFactory setRate(double rate) {
        validate();
        exchange.setRate(rate);
        return this;
    }

    public Exchange generate() {
        validate();
        Exchange generatedExchange = exchange;
        exchange = null;
        return generatedExchange;
    }

    private void validate() {
        if(exchange == null) {
            throw new NullPointerException(EXCHANGE_NULL_EXCEPTION);
        }

        if(exchange.getCurrency() == null) {
            exchange.setCurrency(Currency.EUR);
        }
    }

}
