package com.revolut.test.okawa.fxrate.ui.exchange;

import com.revolut.test.okawa.fxrate.ui.base.BaseView;

/**
 * View used to establish communication between
 * {@link com.revolut.test.okawa.fxrate.presenter.exchange.ExchangePresenter} and
 * {@link ExchangeFragment}
 */
public interface ExchangeView extends BaseView {

}
