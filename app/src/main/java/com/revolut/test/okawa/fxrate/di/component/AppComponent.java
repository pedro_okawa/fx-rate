package com.revolut.test.okawa.fxrate.di.component;

import com.google.gson.Gson;
import com.revolut.test.okawa.fxrate.App;
import com.revolut.test.okawa.fxrate.api.ApiInterface;
import com.revolut.test.okawa.fxrate.di.module.ApiModule;
import com.revolut.test.okawa.fxrate.di.module.AppModule;
import com.revolut.test.okawa.fxrate.di.module.ConnectionModule;
import com.revolut.test.okawa.fxrate.di.module.UtilModule;
import com.revolut.test.okawa.fxrate.util.builder.QueryBuilder;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeFactory;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeReferenceFactory;
import com.revolut.test.okawa.fxrate.util.factory.RxErrorCallAdapterFactory;
import com.revolut.test.okawa.fxrate.util.manager.ApiManager;
import com.revolut.test.okawa.fxrate.util.manager.CallManager;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;
import com.revolut.test.okawa.fxrate.util.manager.NumericConversionManager;
import com.revolut.test.okawa.fxrate.util.receiver.NetworkReceiver;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules = { ApiModule.class, AppModule.class, ConnectionModule.class, UtilModule.class })
public interface AppComponent {

    void inject(App app);
    void inject(NetworkReceiver networkReceiver);

    /* API */
    RxErrorCallAdapterFactory providesRxErrorCallAdapterFactory();
    OkHttpClient providesOkHttpClient();
    Gson providesGson();
    Retrofit providesRetrofit();
    ApiInterface providesApiInterface();
    ApiManager providesApiManager();

    /* APP */
    App providesApp();

    /* CONNECTION */
    ConnectionManager providesConnectionManager();
    NetworkReceiver providesNetworkReceiver();

    /* UTIL */
    CallManager providesCallManager();
    ExchangeFactory providesExchangeFactory();
    ExchangeReferenceFactory providesExchangeReferenceFactory();
    QueryBuilder providesQueryBuilder();
    NumericConversionManager providesNumericConversionManager();

}
