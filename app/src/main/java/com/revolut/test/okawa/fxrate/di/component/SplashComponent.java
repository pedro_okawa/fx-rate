package com.revolut.test.okawa.fxrate.di.component;

import com.revolut.test.okawa.fxrate.di.module.SplashModule;
import com.revolut.test.okawa.fxrate.di.scope.Activity;
import com.revolut.test.okawa.fxrate.presenter.splash.SplashPresenter;
import com.revolut.test.okawa.fxrate.ui.splash.SplashActivity;
import com.revolut.test.okawa.fxrate.ui.splash.SplashView;

import dagger.Component;

@Activity
@Component(dependencies = AppComponent.class, modules = SplashModule.class)
public interface SplashComponent {

    void inject(SplashActivity splashActivity);

    SplashView providesSplashView();
    SplashPresenter providesSplashPresenter();

}
