package com.revolut.test.okawa.fxrate.api;

public class LatestRequestQuery {

    private String base;
    private String symbol;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
