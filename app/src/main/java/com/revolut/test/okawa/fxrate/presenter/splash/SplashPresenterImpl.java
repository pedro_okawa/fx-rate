package com.revolut.test.okawa.fxrate.presenter.splash;

import android.content.Intent;
import android.os.Bundle;

import com.revolut.test.okawa.fxrate.databinding.ActivitySplashBinding;
import com.revolut.test.okawa.fxrate.model.Currency;
import com.revolut.test.okawa.fxrate.presenter.base.ObserverPresenterImpl;
import com.revolut.test.okawa.fxrate.ui.base.BaseActivity;
import com.revolut.test.okawa.fxrate.ui.base.FullScreenActivity;
import com.revolut.test.okawa.fxrate.ui.exchange.ExchangeFragment;
import com.revolut.test.okawa.fxrate.ui.splash.SplashView;
import com.revolut.test.okawa.fxrate.util.builder.ActivityCallBuilder;
import com.revolut.test.okawa.fxrate.util.manager.CallManager;
import com.revolut.test.okawa.fxrate.util.observer.ActivityCallObserver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Splash presenter implementation to handle all logic behind Splash Activity
 */
public class SplashPresenterImpl extends ObserverPresenterImpl implements SplashPresenter {

    private static final long SPLASH_DELAY_SECONDS = 3;

    private ActivityCallBuilder activityCallBuilder;
    private CallManager callManager;
    private SplashView splashView;

    private SplashObserver splashObserver = new SplashObserver();

    public SplashPresenterImpl(CallManager callManager, SplashView splashView) {
        this.callManager = callManager;
        this.splashView = splashView;
    }

    @Override
    public void initialize(ActivitySplashBinding activitySplashBinding) {
        Bundle bundle = new Bundle();

        ArrayList<String> currencies = new ArrayList<>();
        currencies.add(Currency.EUR);
        currencies.add(Currency.GBP);
        currencies.add(Currency.USD);
        bundle.putStringArrayList(ExchangeFragment.BUNDLE_CURRENCIES, currencies);

        /* Stores an instance of the activity call to subscribe with delay */
        activityCallBuilder = callManager
                .activity()
                .init(splashView.getContext())
                .setActivity(FullScreenActivity.class)
                .setFragment(ExchangeFragment.class, BaseActivity.BUNDLE_FRAGMENT)
                .setBundle(bundle)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    @Override
    public void onStart() {
        /* Creates an observable with 3 seconds delay */
        Observable
                .just(activityCallBuilder)
                .delay(SPLASH_DELAY_SECONDS, TimeUnit.SECONDS)
                /* Run on UI thread */
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(splashObserver);
    }

    @Override
    public void onStop() {
        /* Cancel all disposable available on internal list */
        disposeAll();
    }

    private class SplashObserver extends ActivityCallObserver {

        @Override
        public void onSubscribe(Disposable disposable) {
            addDisposable(this, disposable);
        }
    }
}
