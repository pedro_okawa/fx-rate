package com.revolut.test.okawa.fxrate.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.test.okawa.fxrate.BuildConfig;
import com.revolut.test.okawa.fxrate.api.ApiInterface;
import com.revolut.test.okawa.fxrate.util.builder.QueryBuilder;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeReferenceFactory;
import com.revolut.test.okawa.fxrate.util.factory.RxErrorCallAdapterFactory;
import com.revolut.test.okawa.fxrate.util.manager.ApiManager;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module used to define API settings (Gson / OKHTTP Client / RX Adapter / API Interface)
 */
@Module
public class ApiModule {

    /**
     * Returns an instance of RxErrorCallAdapterFactory to enable use observables instead of callbacks
     *
     * @param connectionManager
     * @return RxErrorCallAdapterFactory
     */
    @Singleton
    @Provides
    public RxErrorCallAdapterFactory providesRxErrorCallAdapterFactory(ConnectionManager connectionManager) {
        return new RxErrorCallAdapterFactory(connectionManager);
    }

    /**
     * Instantiate a OkHttpClient object.
     *
     * @return The OkHttpClient.
     */
    @Singleton
    @Provides
    public OkHttpClient providesOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(BuildConfig.API_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(BuildConfig.API_TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(
                        BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.BASIC))
                .build();
    }

    /**
     * Generates custom Gson object with Date converter (yyyy-MM-dd'T'HH:mm:ssZ) implemented
     *
     * @return Gson
     */
    @Singleton
    @Provides
    public Gson providesGson() {
        return new GsonBuilder()
                .setDateFormat(BuildConfig.API_DATE_FORMAT)
                .create();
    }

    /**
     * Generates an instance of retrofit using the following parameters.
     *
     * By the way, you must define the gson to convert date to a specific format, okHttpClient to
     * handle http requests and including logging and rxErrorCallAdapterFactory that will handle
     * server errors and convert to a possible app error
     *
     * @param gson
     * @param okHttpClient
     * @param rxErrorCallAdapterFactory
     * @return Retrofit instance to implement the Api Interface
     */
    @Singleton
    @Provides
    public Retrofit providesRetrofit(Gson gson, OkHttpClient okHttpClient, RxErrorCallAdapterFactory rxErrorCallAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .addCallAdapterFactory(rxErrorCallAdapterFactory)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    /**
     * Creates an api interface to make server request
     *
     * @param retrofit to generate API interface
     * @return API interface
     */
    @Singleton
    @Provides
    public ApiInterface providesApiInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

    /**
     * Generates an instance of API Manager that will handle all request.
     * It will be an interface between user and server requests
     *
     * @param apiInterface that declares all requests
     * @param exchangeReferenceFactory to generate exchange reference objects
     * @param queryBuilder to generate query requests
     * @return API Manager
     */
    @Singleton
    @Provides
    public ApiManager providesApiManager(ApiInterface apiInterface, ExchangeReferenceFactory exchangeReferenceFactory, QueryBuilder queryBuilder) {
        return new ApiManager(apiInterface, exchangeReferenceFactory, queryBuilder);
    }
}

