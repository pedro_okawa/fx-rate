package com.revolut.test.okawa.fxrate.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.revolut.test.okawa.fxrate.App;
import com.revolut.test.okawa.fxrate.di.component.AppComponent;

/**
 * Base activity created to handle basic functions like inflate view, define data binding and inject
 * dependencies
 *
 * @param <T> must be an instance of an object related to {@link ViewDataBinding ViewDataBinding}
 */
public abstract class BaseActivity<T extends ViewDataBinding> extends AppCompatActivity {

    public static final String BUNDLE_FRAGMENT = "BUNDLE_FRAGMENT";

    private static final String APP_CAST_EXCEPTION = "You must define in your manifest your custom App as name of the application";

    private static final int INVALID_CONTAINER_ID = -1;

    private T dataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataBinding = DataBindingUtil.setContentView(this, layoutToInflate());

        inflateInitialFragment();
        injectDependencies(getAppComponent());
        initialize(dataBinding);
    }

    @Override
    protected void onDestroy() {
        if(dataBinding != null) {
            dataBinding.unbind();
        }

        super.onDestroy();
    }

    /**
     * Return layout to be inflated from the child activity
     *
     * @return layoutId
     */
    @LayoutRes
    protected abstract int layoutToInflate();

    /**
     * This method should be used to inject all the necessary dependencies
     *
     * @param appComponent
     */
    protected abstract void injectDependencies(@NonNull AppComponent appComponent);

    /**
     * Initialize the activity with a inflated layout and injected dependencies
     *
     * @param dataBinding
     */
    protected abstract void initialize(@NonNull T dataBinding);

    /**
     * Retrieves the container view to inflate the fragments of the activity
     *
     * @return view container id
     */
    @IdRes
    protected int getContainerViewId() {
        return INVALID_CONTAINER_ID;
    }

    /**
     * Inflate an initial fragment in case the current activity has a
     * container view defined {@link #getContainerViewId()}, has no fragment on back stack
     * and a Fragment defined on bundle
     */
    private void inflateInitialFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0 &&
                getIntent().hasExtra(BUNDLE_FRAGMENT) &&
                getContainerViewId() != INVALID_CONTAINER_ID) {
            String fragmentName = getIntent().getStringExtra(BUNDLE_FRAGMENT);
            replaceFragment(getFragment(fragmentName), fragmentName);
        }
    }

    /**
     * Perform a transaction of a fragment.
     *
     * @param fragment the fragment to be applied.
     * @param tag the tag to be applied.
     */
    protected void replaceFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(getContainerViewId(), fragment, tag);
        fragmentTransaction.commitNow();
    }

    /**
     * Get a new instance of the Fragment by name.
     *
     * @param clazz the canonical Fragment name.
     * @return the instance of the Fragment.
     */
    private Fragment getFragment(String clazz) {
        try {
            Fragment fragment = ((Fragment) Class.forName(clazz).newInstance());

            if (getIntent().getExtras() != null) {
                fragment.setArguments(getIntent().getExtras());
            }

            return fragment;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Retrieve an instance of the custom Application class
     * {@link App app}
     *
     * @return custom {@link App App}
     */
    public App getApp() {
        try {
            return (App) getApplication();
        } catch (ClassCastException exception) {
            throw new ClassCastException(APP_CAST_EXCEPTION);
        }
    }

    /**
     * Retrieve an instance of the {@link AppComponent App Component} that will be used as a
     * dependency to inject all modules
     *
     * @return {@link AppComponent App Component}
     */
    public AppComponent getAppComponent() {
        return getApp().getAppComponent();
    }

}
