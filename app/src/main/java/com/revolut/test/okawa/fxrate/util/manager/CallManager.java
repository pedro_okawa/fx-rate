package com.revolut.test.okawa.fxrate.util.manager;

import com.revolut.test.okawa.fxrate.util.builder.ActivityCallBuilder;
import com.revolut.test.okawa.fxrate.util.builder.FragmentCallBuilder;

/**
 * Manages the screen transactions requests (Activity and Fragment)
 */
public class CallManager {

    private ActivityCallBuilder activityCallBuilder;
    private FragmentCallBuilder fragmentCallBuilder;

    public CallManager() {
        this.activityCallBuilder = new ActivityCallBuilder();
        this.fragmentCallBuilder = new FragmentCallBuilder();
    }

    /**
     * Method used to invoke an activity call builder request
     *
     * @return {@link ActivityCallBuilder}
     */
    public ActivityCallBuilder activity() {
        return activityCallBuilder;
    }

    /**
     * Method used to invoke a fragment call builder request
     *
     * @return {@link FragmentCallBuilder}
     */
    public FragmentCallBuilder fragment() {
        return fragmentCallBuilder;
    }

}
