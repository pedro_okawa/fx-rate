package com.revolut.test.okawa.fxrate.model;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ExchangeReference {

    private PublishSubject<ExchangeReference> exchangeReferenceSubject = PublishSubject.create();

    private Exchange base;
    private Exchange reference;

    public Exchange getBase() {
        return base;
    }

    public void setBase(Exchange base) {
        this.base = base;
    }

    public Exchange getReference() {
        return reference;
    }

    public void setReference(Exchange reference) {
        this.reference = reference;
    }

    public void notifyDataChanged() {
        exchangeReferenceSubject.onNext(this);
    }

    public Observable<ExchangeReference> getExchangeReferenceSubject() {
        return exchangeReferenceSubject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExchangeReference that = (ExchangeReference) o;

        return base.equals(that.base);
    }

    @Override
    public int hashCode() {
        return base.hashCode();
    }
}
