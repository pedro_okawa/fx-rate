package com.revolut.test.okawa.fxrate.util.manager;

import com.revolut.test.okawa.fxrate.api.ApiInterface;
import com.revolut.test.okawa.fxrate.api.LatestRequestQuery;
import com.revolut.test.okawa.fxrate.api.RatesResponse;
import com.revolut.test.okawa.fxrate.model.Currency;
import com.revolut.test.okawa.fxrate.model.ExchangeReference;
import com.revolut.test.okawa.fxrate.util.builder.QueryBuilder;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeReferenceFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Manages all api requests. It's an interface to the api adapter.
 */
public class ApiManager {

    private static final long POLL_DELAY_SECONDS = 0;
    private static final long POLL_INTERVAL_SECONDS = 30;

    private ApiInterface apiInterface;
    private ExchangeReferenceFactory exchangeReferenceFactory;
    private QueryBuilder queryBuilder;

    public ApiManager(ApiInterface apiInterface, ExchangeReferenceFactory exchangeReferenceFactory, QueryBuilder queryBuilder) {
        this.apiInterface = apiInterface;
        this.exchangeReferenceFactory = exchangeReferenceFactory;
        this.queryBuilder = queryBuilder;
    }

    /**
     * Request latest foreign exchange reference rates with a specific base or an specific symbol
     * {@link com.revolut.test.okawa.fxrate.model.Currency Currency }
     *
     * @param callable that holds the query request (It's a callable with an observer to make it variable)
     * @param observer to handle errors, result and state of the request
     */
    public void latest(Callable<ObservableSource<LatestRequestQuery>> callable, Observer<List<ExchangeReference>> observer) {
        Observable.interval(POLL_DELAY_SECONDS, POLL_INTERVAL_SECONDS, TimeUnit.SECONDS, Schedulers.newThread())
                .flatMap(new LatestRequestFunction(callable))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    /**
     * Class used to reference the function to make the request to
     * {@link ApiInterface#latest(Map)} with variable parameters
     */
    private class LatestRequestFunction implements Function<Long, ObservableSource<List<ExchangeReference>>> {

        private Callable<ObservableSource<LatestRequestQuery>> callable;

        LatestRequestFunction(Callable<ObservableSource<LatestRequestQuery>> callable) {
            this.callable = callable;
        }

        @Override
        public ObservableSource<List<ExchangeReference>> apply(Long aLong) throws Exception {
            return Observable
                    .defer(callable)
                    .flatMap(new Function<LatestRequestQuery, ObservableSource<List<ExchangeReference>>>() {
                        @Override
                        public ObservableSource<List<ExchangeReference>> apply(LatestRequestQuery latestRequestQuery) throws Exception {
                            Map<String, String> queryBase = queryBuilder
                                    .init()
                                    .addQuery(ApiInterface.API_QUERY_BASE, latestRequestQuery.getBase())
                                    .addQuery(ApiInterface.API_QUERY_SYMBOLS, latestRequestQuery.getSymbol())
                                    .build();

                            Map<String, String> queryReference = queryBuilder
                                    .init()
                                    .addQuery(ApiInterface.API_QUERY_BASE, latestRequestQuery.getSymbol())
                                    .addQuery(ApiInterface.API_QUERY_SYMBOLS, latestRequestQuery.getBase())
                                    .build();

                            return Observable.combineLatest(apiInterface.latest(queryBase), apiInterface.latest(queryReference),
                                    new BiFunction<RatesResponse, RatesResponse, List<ExchangeReference>>() {
                                        @Override
                                        public List<ExchangeReference> apply(RatesResponse ratesResponseFrom, RatesResponse ratesResponseTo) throws Exception {
                                            List<ExchangeReference> result = new ArrayList<>();

                                            if(ratesResponseFrom.getBase().equals(ratesResponseTo.getBase())) {
                                                result.add(exchangeReferenceFactory.standard(ratesResponseFrom.getBase()));
                                                return result;
                                            }

                                            Map<String, Double> mapRatesFrom = ratesResponseFrom.getRates();
                                            Map<String, Double> mapRatesTo = ratesResponseTo.getRates();

                                            if(!mapRatesFrom.containsKey(ratesResponseTo.getBase()) ||
                                                    !mapRatesTo.containsKey(ratesResponseFrom.getBase())) {
                                                result.add(exchangeReferenceFactory.standard(ratesResponseFrom.getBase()));
                                                result.add(exchangeReferenceFactory.standard(ratesResponseTo.getBase()));
                                                return result;
                                            }

                                            @Currency String referenceCurrencyFrom = ratesResponseTo.getBase();
                                            Double referenceRateFrom = mapRatesFrom.get(ratesResponseTo.getBase());

                                            ExchangeReference exchangeReferenceFrom = exchangeReferenceFactory
                                                    .init()
                                                    .setBase(ratesResponseFrom.getBase())
                                                    .setReference(referenceCurrencyFrom, referenceRateFrom)
                                                    .generate();

                                            @Currency String referenceCurrencyTo = ratesResponseFrom.getBase();
                                            Double referenceRateTo = mapRatesTo.get(ratesResponseFrom.getBase());

                                            ExchangeReference exchangeReferenceTo = exchangeReferenceFactory
                                                    .init()
                                                    .setBase(ratesResponseTo.getBase(), referenceRateFrom)
                                                    .setReference(referenceCurrencyTo, referenceRateTo)
                                                    .generate();

                                            result.add(exchangeReferenceFrom);
                                            result.add(exchangeReferenceTo);

                                            return result;
                                        }
                                    });
                        }
                    });
        }
    }

}
