package com.revolut.test.okawa.fxrate.presenter.exchange;

import android.content.Context;
import android.widget.Toast;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.api.LatestRequestQuery;
import com.revolut.test.okawa.fxrate.databinding.FragmentExchangeBinding;
import com.revolut.test.okawa.fxrate.model.Conversion;
import com.revolut.test.okawa.fxrate.model.Currency;
import com.revolut.test.okawa.fxrate.model.ExchangeReference;
import com.revolut.test.okawa.fxrate.presenter.base.ObserverPresenterImpl;
import com.revolut.test.okawa.fxrate.ui.exchange.ExchangeView;
import com.revolut.test.okawa.fxrate.util.adapter.ExchangePagerAdapter;
import com.revolut.test.okawa.fxrate.util.factory.ExchangeReferenceFactory;
import com.revolut.test.okawa.fxrate.util.manager.ApiManager;
import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;
import com.revolut.test.okawa.fxrate.util.observer.ConnectionObserver;
import com.revolut.test.okawa.fxrate.util.observer.ExchangePageObserver;
import com.revolut.test.okawa.fxrate.util.observer.ExchangeReferenceRequestObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Exchange presenter implementation to handle all logic behind Exchange Fragment
 */
public class ExchangePresenterImpl extends ObserverPresenterImpl implements ExchangePresenter, Callable<ObservableSource<LatestRequestQuery>> {

    public static final double DEFAULT_VALUE = 0.0;

    private PublishSubject<Boolean> baseExchangeSubject = PublishSubject.create();

    private ApiManager apiManager;
    private ConnectionManager connectionManager;
    private ExchangeReferenceFactory exchangeReferenceFactory;
    private ExchangeView exchangeView;

    private List<ExchangeReference> exchangeData;
    private ExchangePagerAdapter fromExchangePagerAdapter;
    private ExchangePagerAdapter toExchangePagerAdapter;
    private FragmentExchangeBinding dataBinding;

    private BaseExchangeObserver baseExchangeObserver;
    private ExchangeConnectionObserver exchangeConnectionObserver;
    private ExchangeReferenceObserver exchangeReferenceObserver;

    private ExchangeReference base;
    private ExchangeReference reference;

    private Conversion conversion = new Conversion();

    public ExchangePresenterImpl(ApiManager apiManager, ConnectionManager connectionManager, ExchangeReferenceFactory exchangeReferenceFactory, ExchangeView exchangeView) {
        this.apiManager = apiManager;
        this.connectionManager = connectionManager;
        this.exchangeReferenceFactory = exchangeReferenceFactory;
        this.exchangeView = exchangeView;
    }

    @Override
    public void initialize(FragmentExchangeBinding dataBinding) {
        this.dataBinding = dataBinding;

        defineViews();
    }

    @Override
    public void onStart() {
        /* Define observers here because I'm disposing them on onStop */
        defineExchangeReferenceObserver();
        defineConnectionObserver();
        defineBaseExchangeObserver();

        updateConversionReferences();
        apiManager.latest(this, exchangeReferenceObserver);
    }

    @Override
    public void onStop() {
        disposeAll();
    }

    @Override
    public ObservableSource<LatestRequestQuery> call() throws Exception {
        LatestRequestQuery latestRequestQuery = new LatestRequestQuery();
        latestRequestQuery.setBase(getBaseCurrency());
        latestRequestQuery.setSymbol(getSymbol());
        return Observable.just(latestRequestQuery);
    }

    @Override
    public void defineCurrencies(List<String> currencies) {
        if(exchangeData == null) {
            exchangeData = new ArrayList<>();
        }

        exchangeData.addAll(exchangeReferenceFactory.standardList(currencies));
    }

    @Override
    public void resetRequest() {
        dispose(exchangeReferenceObserver);
        defineExchangeReferenceObserver();
        defineBaseExchangeObserver();

        updateConversionReferences();
        apiManager.latest(this, exchangeReferenceObserver);
    }

    @Override
    public double getConvertedTotal(double rate) {
        return conversion.getTotal(rate);
    }

    @Override
    public void updateValues(double value) {;
        setTotal(value);
        for(ExchangeReference exchangeReference : exchangeData) {
            exchangeReference.notifyDataChanged();
        }
    }

    @Override
    public void revertValue(double rate) {
        ExchangeReference reverse = base;
        base = reference;
        reference = reverse;
        conversion.setReverse(base.getBase().getRate());
    }

    /**
     * Defines observer to track exchange base changes (if it swap to another currency base)
     */
    private void defineBaseExchangeObserver() {
        dispose(baseExchangeObserver);
        baseExchangeObserver = new BaseExchangeObserver();

        /* Subscribe to receive base exchange reference */
        baseExchangeSubject
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(baseExchangeObserver);
    }

    /**
     * Subscribes an observer to handle connection states
     */
    private void defineConnectionObserver() {
        dispose(exchangeConnectionObserver);
        exchangeConnectionObserver = new ExchangeConnectionObserver();

        /* Subscribe presenter to listen to app connection states */
        connectionManager
                .getConnectionStateObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(exchangeConnectionObserver);
    }

    /**
     * Defines observer to track exchange references changes
     */
    private void defineExchangeReferenceObserver() {
        dispose(exchangeReferenceObserver);
        exchangeReferenceObserver = new ExchangeReferenceObserver(exchangeView.getContext());
    }

    /**
     * Defines total value used to be converted
     *
     * @param value to be converted
     */
    private void setTotal(double value) {
        conversion.setTotal(value);
    }

    /**
     * Defines all values related to the UI
     */
    private void defineViews() {
        fromExchangePagerAdapter = new ExchangePagerAdapter(this);
        fromExchangePagerAdapter.setData(exchangeData);
        fromExchangePagerAdapter.setRequestExchangeSubject(baseExchangeSubject);
        dataBinding.bvwExchangeFrom.setAdapter(fromExchangePagerAdapter);
        dataBinding.crcExchangeFromIndicator.setViewPager(dataBinding.bvwExchangeFrom);
        dataBinding.bvwExchangeFrom.setCurrentItem(fromExchangePagerAdapter.getStartPosition(), false);

        toExchangePagerAdapter = new ExchangePagerAdapter(this);
        toExchangePagerAdapter.setData(exchangeData);
        toExchangePagerAdapter.setRequestExchangeSubject(baseExchangeSubject);
        dataBinding.bvwExchangeTo.setAdapter(toExchangePagerAdapter);
        dataBinding.crcExchangeToIndicator.setViewPager(dataBinding.bvwExchangeTo);
        dataBinding.bvwExchangeTo.setCurrentItem(toExchangePagerAdapter.getStartPosition() + 1, false);

        definePreview();
    }

    /**
     * Retrieves current selected item on from current editMode adapter and return its currency
     * to be used on api request
     *
     * @return currency shown
     */
    private String getBaseCurrency() {
        return base == null || base.getBase() == null ? Currency.EUR : base.getBase().getCurrency();
    }

    /**
     * Retrieve list of currencies available at this point
     *
     * @return array of currencies
     */
    private String getSymbol() {
        return reference == null || reference.getBase() == null ? Currency.GBP : reference.getBase().getCurrency();
    }

    /**
     * Defines the preview at top of the screen
     */
    private void definePreview() {
        updateConversionReferences();
        dataBinding.setBase(base.getReference());
        dataBinding.setReference(reference.getReference());
    }

    /**
     * Retrieves the current item from each adapter, validates which one is on edit mode and then
     * sets the conversion
     */
    private void updateConversionReferences() {
        base = fromExchangePagerAdapter.isOnEditMode() ?
                fromExchangePagerAdapter.getCurrentItem() :
                toExchangePagerAdapter.getCurrentItem();
        reference = fromExchangePagerAdapter.isOnEditMode() ?
                toExchangePagerAdapter.getCurrentItem() :
                fromExchangePagerAdapter.getCurrentItem();
    }

    /**
     * Observer used to handle api requests
     */
    private class ExchangeReferenceObserver extends ExchangeReferenceRequestObserver {

        ExchangeReferenceObserver(Context context) {
            super(context);
        }

        @Override
        public void onSubscribe(Disposable disposable) {
            addDisposable(this, disposable);
        }

        @Override
        protected void onReceiveExchangeReferences(List<ExchangeReference> exchangeReferences) {
            /* Do not override objects from the list, otherwise will lost connections with the observer */
            for(ExchangeReference exchangeReference : exchangeReferences) {
                int index = exchangeData.indexOf(exchangeReference);

                if(index >= 0) {
                    exchangeData.get(index).setBase(exchangeReference.getBase());
                    exchangeData.get(index).setReference(exchangeReference.getReference());
                    exchangeData.get(index).notifyDataChanged();
                }
            }

            definePreview();
        }

        @Override
        protected void onRequestError(String error) {
            Toast.makeText(exchangeView.getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Custom observer used to manage when to reset connection
     */
    private class BaseExchangeObserver extends ExchangePageObserver {

        @Override
        public void onSubscribe(Disposable disposable) {
            addDisposable(this, disposable);
        }

        @Override
        protected void onResetExternal() {
            resetRequest();
        }

        @Override
        protected void onResetInternal() {
            conversion.setTotal(DEFAULT_VALUE);
        }
    }

    /**
     * ConnectionObserver that manages connection state and re-establish connection as soon as it's online
     */
    private class ExchangeConnectionObserver extends ConnectionObserver {

        @Override
        public void onSubscribe(Disposable disposable) {
            addDisposable(this, disposable);
        }

        @Override
        protected void onReconnected() {
            resetRequest();
        }

        @Override
        protected void onDisconnected() {
            Toast.makeText(exchangeView.getContext(), R.string.not_connected, Toast.LENGTH_SHORT).show();
        }
    }
}
