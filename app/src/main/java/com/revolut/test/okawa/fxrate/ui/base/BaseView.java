package com.revolut.test.okawa.fxrate.ui.base;

import android.content.Context;

public interface BaseView {

    /**
     * Retrieves view context
     *
     * @return context of the current view
     */
    Context getContext();

}
