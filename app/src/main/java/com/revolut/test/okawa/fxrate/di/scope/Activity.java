package com.revolut.test.okawa.fxrate.di.scope;

import javax.inject.Scope;

/**
 * Scope used to define components on Activity level
 */
@Scope
public @interface Activity {
}
