package com.revolut.test.okawa.fxrate.model;

/**
 * Object used to hold the data of a specific currency and rate
 */
public class Exchange {

    public static final double BASE_RATE = 1.0;

    private double rate;

    @Currency
    private String currency;

    public void setCurrency(@Currency String currency) {
        this.currency = currency;
    }

    @Currency
    public String getCurrency() {
        return currency;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }

    public String getConvertedRate() {
        return String.valueOf(rate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exchange exchange = (Exchange) o;

        return currency.equals(exchange.currency);
    }

    @Override
    public int hashCode() {
        return currency.hashCode();
    }
}
