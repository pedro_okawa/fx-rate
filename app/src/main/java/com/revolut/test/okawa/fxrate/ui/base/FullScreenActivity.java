package com.revolut.test.okawa.fxrate.ui.base;

import android.support.annotation.NonNull;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.databinding.ActivityFullScreenBinding;
import com.revolut.test.okawa.fxrate.di.component.AppComponent;

/**
 * It's simply a Full Screen Activity used to hold fragments
 */
public class FullScreenActivity extends BaseActivity<ActivityFullScreenBinding> {

    @Override
    protected int layoutToInflate() {
        return R.layout.activity_full_screen;
    }

    @Override
    protected void injectDependencies(@NonNull AppComponent appComponent) {

    }

    @Override
    protected void initialize(@NonNull ActivityFullScreenBinding dataBinding) {

    }

    @Override
    protected int getContainerViewId() {
        return R.id.frmContainer;
    }
}
