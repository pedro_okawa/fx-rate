package com.revolut.test.okawa.fxrate.api;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Interface that refers to API requests
 */
public interface ApiInterface {

    String API_QUERY_BASE = "base";
    String API_QUERY_SYMBOLS = "symbols";

    @GET("latest")
    Observable<RatesResponse> latest(@QueryMap Map<String, String> queryMap);

}
