package com.revolut.test.okawa.fxrate.util.manager;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import com.revolut.test.okawa.fxrate.App;
import com.revolut.test.okawa.fxrate.api.error.RetrofitException;
import com.revolut.test.okawa.fxrate.util.receiver.NetworkReceiver;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Class used to manages connection state
 */
public class ConnectionManager {

    @IntDef({ OFFLINE, ONLINE })
    public @interface ConnectionState {}
    public static final int OFFLINE = 0;
    public static final int ONLINE = 1;

    private App app;
    private int connectionState;
    private NetworkReceiver networkReceiver;

    private PublishSubject<Integer> connectionStateSubject = PublishSubject.create();

    public ConnectionManager(App app, NetworkReceiver networkReceiver) {
        this.app = app;
        this.networkReceiver = networkReceiver;
    }

    /**
     * Register a Broadcast receiver or a NetworkCallback to track connection state
     */
    public void registerConnectivityMonitor() {
        /*
        If device version is inferior to LOLLIPOP it's going to register a BroadcastReceiver
        to check connection state it uses Network Callback to retrieve connection state
         */
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            app.registerReceiver(networkReceiver, intentFilter);
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) app.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerNetworkCallback(new NetworkRequest.Builder().build(), new ConnectionCallback());
        }
    }

    /**
     * Defines connection state based on Broadcast Receiver
     * Attention, this method is called by the NetworkReceiver, no by the Network Callback
     *
     * @param context to retrieve ConnectivityManager
     */
    public void defineConnectionState(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            setConnectionState(activeNetworkInfo != null && activeNetworkInfo.isConnected() ? ONLINE : OFFLINE);
        }
    }

    /**
     * Define connection state value (internet and external - Observable)
     *
     * @param connectionState current connection state
     */
    private void setConnectionState(@ConnectionState int connectionState) {
        this.connectionState = connectionState;
        connectionStateSubject.onNext(connectionState);
    }

    /**
     * Retrieves connection state
     *
     * @return connection state
     */
    public int getConnectionState() {
        return connectionState;
    }

    /**
     * Retrieves connection state observer to keep track on updated state (Used this approach to
     * avoid EventBus)
     *
     * @return Connection State Observable
     */
    public Observable<Integer> getConnectionStateObservable() {
        return connectionStateSubject;
    }

    /**
     * Connection callback used to retrieve network state instead of a Broadcast Receiver
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private class ConnectionCallback extends ConnectivityManager.NetworkCallback {

        @Override
        public void onAvailable(Network network) {
            super.onAvailable(network);
            setConnectionState(ONLINE);
        }

        @Override
        public void onLost(Network network) {
            super.onLost(network);
            setConnectionState(OFFLINE);
        }
    }

    /**
     * Converts a throwable error into a readable app error (In this case, if we use the API with
     * an unknown currency or symbol it returns an error response, but without this call adapter
     * it's possible only to read the following message: HTTP 422 Unprocessable Entity)
     *
     * @param throwable error itself
     * @param retrofit used to convert any http error
     * @return a valid error or exception
     */
    public Exception asRetrofitException(@NonNull Throwable throwable, @NonNull Retrofit retrofit) {
        if(throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            Response response = httpException.response();
            return RetrofitException.httpError(response.raw().request().url().toString(), response, retrofit);
        }

        if(throwable instanceof IOException) {
            return RetrofitException.networkError((IOException) throwable);
        }

        if(throwable instanceof RuntimeException) {
            return RetrofitException.runtimeError((RuntimeException) throwable);
        }

        return RetrofitException.unexpectedError(throwable);
    }
}
