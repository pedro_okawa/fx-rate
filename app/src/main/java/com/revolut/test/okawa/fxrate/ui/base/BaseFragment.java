package com.revolut.test.okawa.fxrate.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.revolut.test.okawa.fxrate.di.component.AppComponent;

/**
 * Base fragment created to handle basic functions like inflate view, define data binding and inject
 * dependencies
 *
 * @param <T> must be an instance of an object related to {@link ViewDataBinding ViewDataBinding}
 */
public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment {

    private static final String BASE_ACTIVITY_CAST_EXCEPTION = "You must use a base activity to implement a base fragment";

    private T dataBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, layoutToInflate(), null, false);

        injectDependencies(getBaseActivity().getAppComponent());
        initialize(dataBinding);

        return dataBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        if(dataBinding != null) {
            dataBinding.unbind();
        }
        super.onDestroyView();
    }

    /**
     * Return layout to be inflated from the child fragment
     *
     * @return layoutId
     */
    @LayoutRes
    protected abstract int layoutToInflate();

    /**
     * This method should be used to inject all the necessary dependencies
     *
     * @param appComponent
     */
    protected abstract void injectDependencies(@NonNull AppComponent appComponent);

    /**
     * Initialize the fragment with a inflated layout and injected dependencies
     *
     * @param dataBinding
     */
    protected abstract void initialize(@NonNull T dataBinding);

    /**
     * Retrieve an instance of the custom Activity class
     * {@link BaseActivity}
     *
     * @return custom {@link BaseActivity}
     */
    private BaseActivity getBaseActivity() {
        try {
            return (BaseActivity) getActivity();
        } catch (ClassCastException exception) {
            throw new ClassCastException(BASE_ACTIVITY_CAST_EXCEPTION);
        }
    }
}
