package com.revolut.test.okawa.fxrate.util.builder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Builder used to generate activity call transactions
 */
public class ActivityCallBuilder extends Builder {

    private static final String CONTEXT_NULL_EXCEPTION = "You must call init() before to define a context to make you're call";
    private static final String CLASS_NULL_EXCEPTION = "You must call setActivity to define an activity to be called";

    private Bundle bundle;
    private String bundleParamName;
    private Context context;
    private Class<? extends AppCompatActivity> activityClass;
    private Class<? extends Fragment> fragmentClass;
    private Integer flags;

    /**
     * Initializes builder setting up values to bundle (it will be used at the end, even empty)
     * and context to start the activity
     *
     * @param context
     * @return Builder instance
     */
    public ActivityCallBuilder init(Context context) {
        this.bundle = new Bundle();
        this.context = context;
        return this;
    }

    /**
     * Defines the activity class to be open.
     *
     * @param clazz activity
     * @return Builder instance
     */
    public ActivityCallBuilder setActivity(Class<? extends AppCompatActivity> clazz) {
        activityClass = clazz;
        return this;
    }

    /**
     * Defines an initial fragment to be added when activity starts, this method will put the
     * fragment class canonical name into the bundle with the given bundle param name and it's
     * necessary to retrieve this fragment from onCreate of the new instantiated activity and
     * inflate inside the layout
     *
     * @param clazz fragment
     * @param bundleParamName to associate the fragment into the bundle
     * @return Builder instance
     */
    public ActivityCallBuilder setFragment(Class<? extends Fragment> clazz, String bundleParamName) {
        fragmentClass = clazz;
        this.bundleParamName = bundleParamName;
        return this;
    }

    /**
     * Defines activity's flags to be used.
     * e.g.: Intent.FLAG_ACTIVITY_NEW_TASK
     *
     * @param flags used to open the activity
     * @return Builder instance
     */
    public ActivityCallBuilder setFlags(int flags) {
        this.flags = flags;
        return this;
    }

    /**
     * Defines bundle to be used by the opened activity or fragment
     *
     * @param bundle arguments
     * @return Builder instance
     */
    public ActivityCallBuilder setBundle(Bundle bundle) {
        if(bundle != null) {
            this.bundle = bundle;
        }
        return this;
    }

    @Override
    public void build() {
        validate();

        Intent intent = new Intent(context, activityClass);

        if(flags != null) {
            intent.setFlags(flags);
        }

        if(fragmentClass != null) {
            bundle.putString(bundleParamName, fragmentClass.getCanonicalName());
        }

        intent.putExtras(bundle);

        context.startActivity(intent);

        reset();
    }

    @Override
    protected void validate() {
        if(context == null) {
            throw new NullPointerException(CONTEXT_NULL_EXCEPTION);
        }

        if(activityClass == null) {
            throw new NullPointerException(CLASS_NULL_EXCEPTION);
        }
    }

    @Override
    protected void reset() {
        bundle = null;
        context = null;
        activityClass = null;
        fragmentClass = null;
        flags = null;
    }
}
