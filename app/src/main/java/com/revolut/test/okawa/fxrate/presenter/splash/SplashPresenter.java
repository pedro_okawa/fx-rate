package com.revolut.test.okawa.fxrate.presenter.splash;

import com.revolut.test.okawa.fxrate.databinding.ActivitySplashBinding;
import com.revolut.test.okawa.fxrate.presenter.base.BasePresenter;

/**
 * Splash presenter interface used to communicate
 * {@link com.revolut.test.okawa.fxrate.ui.splash.SplashActivity } and
 * {@link SplashPresenterImpl}
 */
public interface SplashPresenter extends BasePresenter<ActivitySplashBinding> {

}
