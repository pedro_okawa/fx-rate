package com.revolut.test.okawa.fxrate.presenter.base;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ObserverPresenterImpl {

    private Map<Observer, Disposable> disposables = new HashMap<>();

    /**
     * Add a disposable into the internal list to cancel all requests when call onStop
     *
     * @param disposable
     */
    public void addDisposable(Observer observer, Disposable disposable) {
        disposables.put(observer, disposable);
    }

    /**
     * Disposes the specific request
     *
     * @param observer
     */
    public void dispose(Observer observer) {
        if(disposables.containsKey(observer)) {
            disposables.remove(observer).dispose();
        }
    }

    /**
     * Interrupt/cancel all added disposables
     */
    public void disposeAll() {
        for(Map.Entry<Observer, Disposable> entry : disposables.entrySet()) {
            entry.getValue().dispose();
        }

        disposables.clear();
    }

}
