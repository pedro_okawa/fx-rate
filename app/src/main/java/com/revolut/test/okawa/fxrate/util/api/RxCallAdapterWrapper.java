package com.revolut.test.okawa.fxrate.util.api;

import com.revolut.test.okawa.fxrate.util.manager.ConnectionManager;

import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * Created by okawa on 15-08-2017.
 */
public class RxCallAdapterWrapper implements CallAdapter<Call<?>, Observable<?>> {

    private CallAdapter<?, ?> wrapper;
    private ConnectionManager connectionManager;
    private Retrofit retrofit;

    public RxCallAdapterWrapper(CallAdapter<?, ?> wrapper, ConnectionManager connectionManager, Retrofit retrofit) {
        this.wrapper = wrapper;
        this.connectionManager = connectionManager;
        this.retrofit = retrofit;
    }

    @Override
    public Type responseType() {
        return wrapper.responseType();
    }

    @Override
    public Observable<?> adapt(Call call) {
        return ((Observable) wrapper.adapt(call)).onErrorResumeNext(new Function<Throwable, ObservableSource>() {
            @Override
            public ObservableSource apply(Throwable throwable) throws Exception {
                return Observable.error(connectionManager.asRetrofitException(throwable, retrofit));
            }
        });
    }
}
