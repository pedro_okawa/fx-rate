package com.revolut.test.okawa.fxrate.util.observer;

import android.content.Context;

import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.api.error.ApiError;
import com.revolut.test.okawa.fxrate.api.error.RetrofitException;

import java.io.IOException;

import io.reactivex.Observer;

/**
 * Observer created to handle requests that returns some value
 *
 * @param <T> type of the object expected
 */
public abstract class RequestObserver<T> implements Observer<T> {

    private Context context;

    public RequestObserver(Context context) {
        this.context = context;
    }

    protected abstract void onReceive(T t);

    protected abstract void onRequestError(String error);

    @Override
    public final void onNext(T value) {
        if (value == null) {
            return;
        }

        onReceive(value);
    }

    @Override
    public final void onError(Throwable throwable) {
        RetrofitException retrofitException = (RetrofitException) throwable;

        if (retrofitException.getType() != RetrofitException.Type.HTTP) {
            onRequestError(context.getString(R.string.unexpected_error));
            return;
        }

        try {
            ApiError apiError = convertApiError(retrofitException);

            onRequestError(apiError.getError());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void onComplete () {

    }

    /**
     * Converts throwable to an Api Error instance
     *
     * @param retrofitException
     * @return ApiError
     */
    private ApiError convertApiError(RetrofitException retrofitException) throws IOException {
        return retrofitException.getErrorBodyAs(ApiError.class);
    }
}
