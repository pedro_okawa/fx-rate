package com.revolut.test.okawa.fxrate.util;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.support.test.espresso.IdlingResource;

import com.revolut.test.okawa.fxrate.ui.splash.SplashActivity;

public class SplashIdlingResource implements IdlingResource {

    private Context context;
    private ResourceCallback resourceCallback;

    public SplashIdlingResource(Context context) {
        this.context = context;
    }

    @Override
    public String getName() {
        return SplashIdlingResource.class.getName();
    }

    @Override
    public boolean isIdleNow() {
        boolean idle = !isSplashRunning();
        if(idle && resourceCallback != null) {
            resourceCallback.onTransitionToIdle();
        }
        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    private boolean isSplashRunning() {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for(ActivityManager.AppTask appTask : activityManager.getAppTasks()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(SplashActivity.class.getName().equals(appTask.getTaskInfo().topActivity.getClassName())) {
                        return true;
                    }
                    continue;
                }

                if(SplashActivity.class.getName().equals(appTask.getTaskInfo().origActivity.getClassName())) {
                    return true;
                }
            }
        } else {
            for(ActivityManager.RunningTaskInfo runningTaskInfo : activityManager.getRunningTasks(Integer.MAX_VALUE)) {
                if(SplashActivity.class.getName().equals(runningTaskInfo.topActivity.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }
}

