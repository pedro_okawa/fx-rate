package com.revolut.test.okawa.fxrate.di.module;

import com.revolut.test.okawa.fxrate.api.ApiInterface;

import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;

public class ApiTestModule extends ApiModule {

    @Override
    public ApiInterface providesApiInterface(Retrofit retrofit) {
        return mock(ApiInterface.class);
    }
}
