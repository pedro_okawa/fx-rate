package com.revolut.test.okawa.fxrate.di.module;

import com.google.gson.Gson;
import com.revolut.test.okawa.fxrate.util.TestHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TestModule {

    @Singleton
    @Provides
    public TestHelper providesTestHelper(Gson gson) {
        return new TestHelper(gson);
    }

}
