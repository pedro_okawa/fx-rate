package com.revolut.test.okawa.fxrate.suite;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.revolut.test.okawa.fxrate.AppTest;
import com.revolut.test.okawa.fxrate.R;
import com.revolut.test.okawa.fxrate.api.ApiInterface;
import com.revolut.test.okawa.fxrate.model.Currency;
import com.revolut.test.okawa.fxrate.model.Exchange;
import com.revolut.test.okawa.fxrate.ui.base.BaseActivity;
import com.revolut.test.okawa.fxrate.ui.base.FullScreenActivity;
import com.revolut.test.okawa.fxrate.ui.exchange.ExchangeFragment;
import com.revolut.test.okawa.fxrate.util.Constants;
import com.revolut.test.okawa.fxrate.util.TestHelper;
import com.revolut.test.okawa.fxrate.util.manager.CallManager;
import com.revolut.test.okawa.fxrate.util.manager.NumericConversionManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import javax.inject.Inject;

import static android.os.SystemClock.sleep;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExchangeFragmentTest {

    private static final int INITIAL_PAGE_VIEW_PAGER = Integer.MAX_VALUE / 2;

    private static final double INPUT_TOTAL_VALUE_BASE = 10;
    private static final double INPUT_TOTAL_VALUE_REFERENCE = 5.75;

    @Inject
    ApiInterface apiInterface;
    @Inject
    CallManager callManager;
    @Inject
    NumericConversionManager numericConversionManager;
    @Inject
    TestHelper testHelper;

    private ArrayList<String> currencies = new ArrayList<>();

    @Rule
    public ActivityTestRule<FullScreenActivity> activityRule = new ActivityTestRule<>(FullScreenActivity.class, true, false);

    @Before
    public void initialize() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        AppTest appTest = AppTest.class.cast(instrumentation.getTargetContext().getApplicationContext());

        appTest.getAppTestComponent().inject(this);

        testHelper.mockRequest(apiInterface, Currency.EUR, Currency.EUR, Constants.RESPONSE_EUR_EUR);
        testHelper.mockRequest(apiInterface, Currency.EUR, Currency.GBP, Constants.RESPONSE_EUR_GBP);
        testHelper.mockRequest(apiInterface, Currency.EUR, Currency.USD, Constants.RESPONSE_EUR_USD);
        testHelper.mockRequest(apiInterface, Currency.GBP, Currency.GBP, Constants.RESPONSE_GBP_GBP);
        testHelper.mockRequest(apiInterface, Currency.GBP, Currency.EUR, Constants.RESPONSE_GBP_EUR);
        testHelper.mockRequest(apiInterface, Currency.GBP, Currency.USD, Constants.RESPONSE_GBP_USD);
        testHelper.mockRequest(apiInterface, Currency.USD, Currency.USD, Constants.RESPONSE_USD_USD);
        testHelper.mockRequest(apiInterface, Currency.USD, Currency.EUR, Constants.RESPONSE_USD_EUR);
        testHelper.mockRequest(apiInterface, Currency.USD, Currency.GBP, Constants.RESPONSE_USD_GBP);

        Intent intent = new Intent();

        currencies.add(Currency.EUR);
        currencies.add(Currency.GBP);
        currencies.add(Currency.USD);
        intent.putExtra(ExchangeFragment.BUNDLE_CURRENCIES, currencies);
        intent.putExtra(BaseActivity.BUNDLE_FRAGMENT, ExchangeFragment.class.getCanonicalName());

        activityRule.launchActivity(intent);
    }

    /**
     * Check if an input on any currency with the same will return the same value
     */
    @Test
    public void checkSameValues() {
        String result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Exchange.BASE_RATE);

        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.GBP)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.GBP)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        testHelper.fillEditText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_BASE));

        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        testHelper.checkText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, result);
    }

    /**
     * Check conversion from euros to pounds
     */
    @Test
    public void checkEurToGbpConversion() {
        String result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Constants.RATE_EUR_TO_GBP);

        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.GBP)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* FILL BASE EDIT TEXT WITH PRE DEFINED TEXT */
        testHelper.fillEditText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_BASE));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CHECK IF THE RESULT MATCHES TO THE CALCULUS */
        testHelper.checkText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, result);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON REFERENCE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeTo, R.id.crnExchangePageValue);

        /* VALIDATES IF THE REVERSION IS OK */
        result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Exchange.BASE_RATE);
        testHelper.checkText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, result);

        /* FILL REFERENCE EDIT TEXT WITH PRE DEFINED TEXT */
        testHelper.fillEditText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_REFERENCE));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE REVERSION IS OK */
        result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_REFERENCE * Constants.RATE_GBP_TO_EUR);
        testHelper.checkText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, result);
    }

    /**
     * Check conversion from euros to dollars
     */
    @Test
    public void checkEurToUsdConversion() {
        String result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Constants.RATE_EUR_TO_USD);

        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        /* IT WAS NECESSARY TO SCROLL TO THE SECOND PAGE AD AFTERWARDS TO THE THIRD PAGE */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.GBP)));
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.USD)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* FILL BASE EDIT TEXT WITH PRE DEFINED TEXT */
        testHelper.fillEditText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_BASE));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CHECK IF THE RESULT MATCHES TO THE CALCULUS */
        testHelper.checkText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, result);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON REFERENCE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeTo, R.id.crnExchangePageValue);

        /* VALIDATES IF THE REVERSION IS OK */
        result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Exchange.BASE_RATE);
        testHelper.checkText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, result);

        /* FILL REFERENCE EDIT TEXT WITH PRE DEFINED TEXT */
        testHelper.fillEditText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_REFERENCE));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE REVERSION IS OK */
        result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_REFERENCE * Constants.RATE_USD_TO_EUR);
        testHelper.checkText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, result);
    }

    /**
     * Check conversion from pounds to dollars
     */
    @Test
    public void checkGbpToUsdConversion() {
        String result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Constants.RATE_GBP_TO_USD);

        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.GBP)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        /* IT WAS NECESSARY TO SCROLL TO THE SECOND PAGE AD AFTERWARDS TO THE THIRD PAGE */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.GBP)));
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.USD)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* FILL BASE EDIT TEXT WITH PRE DEFINED TEXT */
        testHelper.fillEditText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_BASE));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CHECK IF THE RESULT MATCHES TO THE CALCULUS */
        testHelper.checkText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, result);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON REFERENCE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeTo, R.id.crnExchangePageValue);

        /* VALIDATES IF THE REVERSION IS OK */
        result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_BASE * Exchange.BASE_RATE);
        testHelper.checkText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, result);

        /* FILL REFERENCE EDIT TEXT WITH PRE DEFINED TEXT */
        testHelper.fillEditText(R.id.bvwExchangeTo, R.id.crnExchangePageValue, String.valueOf(INPUT_TOTAL_VALUE_REFERENCE));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE REVERSION IS OK */
        result = numericConversionManager.fromDouble(INPUT_TOTAL_VALUE_REFERENCE * Constants.RATE_USD_TO_GBP);
        testHelper.checkText(R.id.bvwExchangeFrom, R.id.crnExchangePageValue, result);
    }

    @Test
    public void checkIfHideConversionWhenFocused() {
        /* CLICK ON BASE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeFrom, R.id.crnExchangePageValue);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE CONVERSION VIEW IS NOT VISIBLE ON BASE */
        testHelper.checkIsHidden(R.id.bvwExchangeFrom, R.id.txtExchangePageConversion);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON REFERENCE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeTo, R.id.crnExchangePageValue);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE CONVERSION VIEW IS NOT VISIBLE ON REFERENCE */
        testHelper.checkIsHidden(R.id.bvwExchangeTo, R.id.txtExchangePageConversion);
    }

    @Test
    public void checkIfHideBothConversionsWithSameCurrency() {
        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE CONVERSION VIEW IS NOT VISIBLE ON BASE */
        testHelper.checkIsHidden(R.id.bvwExchangeFrom, R.id.txtExchangePageConversion);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE CONVERSION VIEW IS NOT VISIBLE ON BASE */
        testHelper.checkIsHidden(R.id.bvwExchangeTo, R.id.txtExchangePageConversion);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
    }

    @Test
    public void checkIfHidePreviewWithSameCurrency() {
        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* VALIDATES IF THE CONVERSION VIEW IS NOT VISIBLE ON BASE */
        testHelper.checkIsHidden(R.id.txtExchangePreview);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
    }

    @Test
    public void checkPreviewText() {
        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.GBP)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON BASE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeFrom, R.id.crnExchangePageValue);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        testHelper.checkText(R.id.txtExchangePreview, generateConversionString(Currency.EUR, Currency.GBP, Constants.RATE_EUR_TO_GBP));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON BASE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeTo, R.id.crnExchangePageValue);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        testHelper.checkText(R.id.txtExchangePreview, generateConversionString(Currency.GBP, Currency.EUR, Constants.RATE_GBP_TO_EUR));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
    }

    @Test
    public void checkIfConversionTextIsCorrect() {
        /* SCROLL TO EURO AND POUND PAGES WITH A SMALL DELAY */
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeFrom, calculatePage(currencies.indexOf(Currency.EUR)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);
        testHelper.swipeViewPagerToPage(R.id.bvwExchangeTo, calculatePage(currencies.indexOf(Currency.GBP)));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON BASE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeFrom, R.id.crnExchangePageValue);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        testHelper.checkText(R.id.bvwExchangeTo, R.id.txtExchangePageConversion, generateConversionString(Currency.GBP, Currency.EUR, Constants.RATE_GBP_TO_EUR));
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        /* CLICK ON BASE EDIT TEXT */
        testHelper.clickView(R.id.bvwExchangeTo, R.id.crnExchangePageValue);
        sleep(Constants.TEST_DELAY_INTERVAL_SMALL);

        testHelper.checkText(R.id.bvwExchangeFrom, R.id.txtExchangePageConversion, generateConversionString(Currency.EUR, Currency.GBP, Constants.RATE_EUR_TO_GBP));
    }

    @After
    public void dispose() {
        currencies.clear();
    }

    private int calculatePage(int page) {
        return INITIAL_PAGE_VIEW_PAGER + page;
    }

    private String generateConversionString(@Currency String base, @Currency String reference, double rate) {
        String baseSymbol = java.util.Currency.getInstance(base).getSymbol();
        String referenceSymbol = java.util.Currency.getInstance(reference).getSymbol();

        return activityRule.getActivity().getString(R.string.exchange_conversion, baseSymbol, referenceSymbol, String.valueOf(rate));
    }
}
