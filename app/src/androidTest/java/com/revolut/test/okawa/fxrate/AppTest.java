package com.revolut.test.okawa.fxrate;

import com.revolut.test.okawa.fxrate.di.component.AppComponent;
import com.revolut.test.okawa.fxrate.di.component.AppTestComponent;
import com.revolut.test.okawa.fxrate.di.component.DaggerAppTestComponent;
import com.revolut.test.okawa.fxrate.di.module.ApiTestModule;
import com.revolut.test.okawa.fxrate.di.module.AppModule;

public class AppTest extends App {

    @Override
    protected AppComponent initializeComponent() {
        AppTestComponent appTestComponent = DaggerAppTestComponent
                .builder()
                .apiModule(new ApiTestModule())
                .appModule(new AppModule(this))
                .build();

        appTestComponent.inject(this);

        return appTestComponent;
    }

    public AppTestComponent getAppTestComponent() {
        return AppTestComponent.class.cast(getAppComponent());
    }

}
