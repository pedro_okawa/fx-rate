package com.revolut.test.okawa.fxrate.util;

import android.support.annotation.IdRes;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;

import com.google.gson.Gson;
import com.revolut.test.okawa.fxrate.api.ApiInterface;
import com.revolut.test.okawa.fxrate.api.RatesResponse;
import com.revolut.test.okawa.fxrate.model.Currency;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.ViewPagerActions.scrollToPage;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.mockito.Mockito.when;

public class TestHelper {

    private Gson gson;

    public TestHelper(Gson gson) {
        this.gson = gson;
    }

    public void fillEditText(@IdRes int editTextId, String value) {
        onView(withId(editTextId))
                .perform(clearText())
                .perform(typeText(value));
        Espresso.closeSoftKeyboard();
    }

    public void fillEditText(@IdRes int parentId, @IdRes int editTextId, String value) {
        onView(allOf(withId(editTextId),
                allOf(isDescendantOfA(withId(parentId)), isDisplayed())))
                .perform(clearText())
                .perform(typeText(value));
        Espresso.closeSoftKeyboard();
    }

    public void swipeViewPagerLeft(@IdRes int viewPagerId) {
        onView(withId(viewPagerId)).perform(swipeLeft());
    }

    public void swipeViewPagerRight(@IdRes int viewPagerId) {
        onView(withId(viewPagerId)).perform(swipeRight());
    }

    public void swipeViewPagerToPage(@IdRes int viewPagerId, int page) {
        onView(withId(viewPagerId)).perform(scrollToPage(page, false));
    }

    public void checkText(@IdRes int viewId, String text) {
        onView(withId(viewId)).check(matches(withText(text)));
    }

    public void checkText(@IdRes int parentId, @IdRes int childId, String text) {
        onView(allOf(withId(childId),
                allOf(isDescendantOfA(withId(parentId)), isDisplayed())))
                .check(matches(withText(text)));
    }

    public void clickView(@IdRes int viewId) {
        onView(allOf(withId(viewId), isDisplayed())).perform(click());
    }

    public void clickView(@IdRes int parentId, @IdRes int childId) {
        onView(allOf(withId(childId),
                allOf(isDescendantOfA(withId(parentId)), isDisplayed())))
                .perform(click());
    }

    public void checkIsHidden(@IdRes int viewId) {
        onView(withId(viewId))
                .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    public void checkIsHidden(@IdRes int parentId, @IdRes int childId) {
        onView(withId(parentId))
                .check(matches(hasDescendant(allOf(withId(childId), withEffectiveVisibility(ViewMatchers.Visibility.GONE)))));
    }

    /**
     * Generates a mock request based on parameters used and given file path
     *
     * @param apiInterface to be mocked
     * @param base of the exchange
     * @param symbol to be converted
     * @param filePath of the mocked file
     */
    public void mockRequest(ApiInterface apiInterface, @Currency String base, @Currency String symbol, String filePath) {
        Map<String, String> query = new HashMap<>();
        query.put(ApiInterface.API_QUERY_BASE, base);
        query.put(ApiInterface.API_QUERY_SYMBOLS, symbol);

        Observable<RatesResponse> ratesResponseObservable = generateMockedObservable(filePath, RatesResponse.class);

        when(apiInterface.latest(query)).thenReturn(ratesResponseObservable);
    }

    private  <T> Observable<T> generateMockedObservable(String response, Class<T> clazz) {
        T t = gson.fromJson(response, clazz);
        return Observable.just(t);
    }

}
