package com.revolut.test.okawa.fxrate.di.component;

import com.revolut.test.okawa.fxrate.di.module.ApiModule;
import com.revolut.test.okawa.fxrate.di.module.AppModule;
import com.revolut.test.okawa.fxrate.di.module.ConnectionModule;
import com.revolut.test.okawa.fxrate.di.module.TestModule;
import com.revolut.test.okawa.fxrate.di.module.UtilModule;
import com.revolut.test.okawa.fxrate.suite.ExchangeFragmentTest;
import com.revolut.test.okawa.fxrate.util.TestHelper;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { ApiModule.class, AppModule.class, ConnectionModule.class, UtilModule.class, TestModule.class})
public interface AppTestComponent extends AppComponent {

    void inject(ExchangeFragmentTest exchangeFragmentTest);

    TestHelper providesTestHelper();

}
