package com.revolut.test.okawa.fxrate.util;

public class Constants {

    public static final int TEST_DELAY_INTERVAL_SMALL = 1000;
    public static final int TEST_DELAY_INTERVAL_MEDIUM = 2500;
    public static final int TEST_DELAY_INTERVAL_LARGE = 5000;
    
    public static final double RATE_EUR_TO_GBP = 0.90993;
    public static final double RATE_EUR_TO_USD = 1.171;
    public static final double RATE_GBP_TO_EUR = 1.099;
    public static final double RATE_GBP_TO_USD = 1.2869;
    public static final double RATE_USD_TO_EUR = 0.85397;
    public static final double RATE_USD_TO_GBP = 0.77705;

    /**
     * RESPONSES
     */

    public static final String RESPONSE_EUR_EUR = "{\n" +
            "\t\"base\": \"EUR\",\n" +
            "\t\"date\": \"2017-08-16\",\n" +
            "\t\"rates\": {}\n" +
            "}";

    public static final String RESPONSE_GBP_GBP = "{\n" +
            "\t\"base\": \"GBP\",\n" +
            "\t\"date\": \"2017-08-16\",\n" +
            "\t\"rates\": {}\n" +
            "}";

    public static final String RESPONSE_USD_USD = "{\n" +
            "\t\"base\": \"USD\",\n" +
            "\t\"date\": \"2017-08-16\",\n" +
            "\t\"rates\": {}\n" +
            "}";

    public static final String RESPONSE_EUR_GBP = "{\n" +
            "  \"base\": \"EUR\",\n" +
            "  \"date\": \"2017-08-16\",\n" +
            "  \"rates\": {\n" +
            "    \"GBP\": " + RATE_EUR_TO_GBP + "\n" +
            "  }\n" +
            "}";

    public static final String RESPONSE_EUR_USD = "{\n" +
            "  \"base\": \"EUR\",\n" +
            "  \"date\": \"2017-08-16\",\n" +
            "  \"rates\": {\n" +
            "    \"USD\": " + RATE_EUR_TO_USD + "\n" +
            "  }\n" +
            "}";

    public static final String RESPONSE_GBP_EUR = "{\n" +
            "  \"base\": \"GBP\",\n" +
            "  \"date\": \"2017-08-16\",\n" +
            "  \"rates\": {\n" +
            "    \"EUR\": " + RATE_GBP_TO_EUR + "\n" +
            "  }\n" +
            "}";

    public static final String RESPONSE_GBP_USD = "{\n" +
            "  \"base\": \"GBP\",\n" +
            "  \"date\": \"2017-08-16\",\n" +
            "  \"rates\": {\n" +
            "    \"USD\": " + RATE_GBP_TO_USD + "\n" +
            "  }\n" +
            "}";

    public static final String RESPONSE_USD_EUR = "{\n" +
            "  \"base\": \"USD\",\n" +
            "  \"date\": \"2017-08-16\",\n" +
            "  \"rates\": {\n" +
            "    \"EUR\": " + RATE_USD_TO_EUR + "\n" +
            "  }\n" +
            "}";

    public static final String RESPONSE_USD_GBP = "{\n" +
            "  \"base\": \"USD\",\n" +
            "  \"date\": \"2017-08-16\",\n" +
            "  \"rates\": {\n" +
            "    \"GBP\": " + RATE_USD_TO_GBP + "\n" +
            "  }\n" +
            "}";

}
